package com.monash.View;

import com.monash.model.Job;
import com.monash.model.User;

import javax.swing.*;
import javax.swing.plaf.metal.MetalButtonUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Provides display of the jobs that the recruiter has created
 */
public class RecruiterJobSummary {
    private boolean showOpen;
    private JPanel rootPanel;
    private JTextArea heading;
    private JTextArea subHeading;
    private JPanel jobsList;
    private JScrollPane jobScrollView;
    private JTextArea jobTitle;
    private JPanel headerRow;
    private JPanel content;
    private JButton newJob;
    private JTextArea dateCreated;
    private JTextArea applicants;

    /**
     * Default constructor - defaults to displaying closed jobs.
     */
    public RecruiterJobSummary(){
        newJob.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Create new job");
                JobForm form = new JobForm();

                form.setBackButtonToOpenJobs(showOpen);
                FrameManager.getInstance().swapIntoPanel((JPanel) rootPanel.getParent(), form.getRootPanel());
            }
        });

        updateGUI();
    }


    /**
     * Constructor to designate showing of open or closed jobs.
     * @param showOpen
     */
    public RecruiterJobSummary(Boolean showOpen){
        this();
        this.showOpen = showOpen;
        updateGUI();
    }

    /**
     * Gets the root JPanel to display the screen.
     * @return root JPanel.
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }

    /**
     * Generates the display of single-lined job detail summaries for jobs created by the recruiter.
     */
    private void updateGUI() {
        jobScrollView.setBorder(BorderFactory.createEmptyBorder());
        jobsList.setLayout(new BoxLayout(jobsList,BoxLayout.Y_AXIS));

        heading.setText((showOpen) ? "Open Jobs" : "Closed Jobs");
        subHeading.setText((showOpen) ? "Select a job below to manage currently open job advertisements" : "Select a job below to manage currently closed job advertisements");
        jobsList.removeAll();

        populateJobs();

        jobScrollView.getVerticalScrollBar().setUnitIncrement(20);

        jobScrollView.revalidate();
        jobScrollView.repaint();

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                jobScrollView.getVerticalScrollBar().setValue(0);
            }
        });

    }

    private void populateJobs() {
        rootPanel.repaint();
        ArrayList<Job> panelJobList = new ArrayList<>();
        panelJobList.clear();
        panelJobList = FrameManager.getInstance().getController().getRecruiterJobList();

        for (Job job: panelJobList) {
            if(job.isActive() != showOpen){ continue; }

            ArrayList<User> applicantList = new ArrayList<>();
            var panel = new JobDetailsSummary();
            panel.getJobTitle().setText(job.getTitle());
            try {
                applicantList = FrameManager.getInstance().getController().getJobApplicants(job.getJobId());
                panel.getApplicants().setText(String.valueOf(applicantList.size()));
            } catch (SQLException e) {
                e.printStackTrace();
                panel.getApplicants().setText("-1");
            }

            panel.getDateCreated().setText((job.getDate() != null) ? job.getDate().toString() : "DD MMM YYYY");
            panel.getEdit().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JobForm form = null;
                    try {
                        form = new JobForm(JobForm.Mode.EDIT, job);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                    form.setBackButtonToOpenJobs(showOpen);
                    FrameManager.getInstance().swapIntoPanel((JPanel) rootPanel.getParent(), form.getRootPanel());
                }
            });

            panel.getView().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    RecruiterJobApplicants applicants = null;

                    try {
                        applicants = new RecruiterJobApplicants(job,FrameManager.getInstance().getController().getJobApplicants(job.getJobId()));
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                    //applicants = new RecruiterJobApplicants();


                    applicants.setBackButtonToOpenJobs(showOpen);

                    FrameManager.getInstance().swapIntoPanel((JPanel) rootPanel.getParent(), applicants.getRootPanel());
                }
            });

            if(applicantList.isEmpty()){
                panel.getView().setEnabled(false);

                //UI override to change the LaF of disabled button, to completely hide
                panel.getView().setUI(new MetalButtonUI() {
                    protected Color getDisabledTextColor() {
                        return Color.WHITE;
                    }
                });
            }

            jobsList.add(panel.getRootPanel());
            jobsList.add(Box.createRigidArea(new Dimension(-1, 10)));

        }
    }
}

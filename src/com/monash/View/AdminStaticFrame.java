package com.monash.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Generates the static outer frame used for GUI display to seekers.
 * @author Lea Walton
 */
public class AdminStaticFrame {
    //Card names
    final static String CATEGORIES = "Categories";
    final static String PASS = "Password";

    private JPanel contentPanel;
    private JButton config;
    private JPanel rootPanel;
    private JLabel logoJSS;
    private JPanel categoriesLine;
    private JButton signout;
    private JButton settings;
    private JTextArea administrationTextArea;
    private JPanel settingsLine;
    private JTextArea user;
    private JButton accounts;

    /**
     * Default constructor
     */
    public AdminStaticFrame() {

        ImageIcon jssLogo = new ImageIcon("resources/Jss_logo_short.png");
        this.logoJSS.setIcon(jssLogo);

        user.setText("User: " + " root admin"); //TODO: update with account name when admin account added to database

        categoriesLine.setVisible(true);
        settingsLine.setVisible(false);

        AdminConfig cat = new AdminConfig();
        Password pw = new Password();

        contentPanel.add(cat.getRootPanel(), CATEGORIES);
        contentPanel.add(pw.getRootPanel(), PASS);

        CardLayout cl = (CardLayout)(contentPanel.getLayout());


        config.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                categoriesLine.setVisible(true);

                cl.show(contentPanel,CATEGORIES);
                categoriesLine.setVisible(true);
                settingsLine.setVisible(false);
            }
        });
        signout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrameManager.getInstance().getController().setUser(null);
                MainLogin login = new MainLogin();
                FrameManager.getInstance().getMainFrame().setContentPane(login.getRootPanel());
                FrameManager.getInstance().getMainFrame().revalidate();
                FrameManager.getInstance().getMainFrame().repaint();
                FrameManager.getInstance().getController().clearData();
            }
        });
        settings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                categoriesLine.setVisible(false);
                settingsLine.setVisible(true);
                cl.show(contentPanel, PASS);
            }
        });
    }

    /**
     * Gets the root JPanel that holds the display for the static frame.
     * @return  the root JPanel
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }
}

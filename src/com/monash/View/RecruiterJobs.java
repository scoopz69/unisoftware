package com.monash.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RecruiterJobs {
    //Card names
    final static String OPEN = "Open Jobs";
    final static String CLOSED = "Closed Jobs";
    final static String CATEGORIES = "Categories";

    private JPanel rootPanel;
    private JPanel contentPanel;
    private JPanel sidebar;
    private JButton openJobs;
    private JButton closedJobs;
    private JButton categories;

    public RecruiterJobs() {
        RecruiterJobSummary open = new RecruiterJobSummary(true);
        contentPanel.add(open.getRootPanel());

        ToggleButtonColors(openJobs);

        openJobs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ToggleButtonColors(openJobs);
                RecruiterJobSummary open = new RecruiterJobSummary(true);
                contentPanel.removeAll();
                contentPanel.add(open.getRootPanel());
                contentPanel.revalidate();
                contentPanel.repaint();
            }
        });
        closedJobs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ToggleButtonColors(closedJobs);
                RecruiterJobSummary closed = new RecruiterJobSummary(false);
                contentPanel.removeAll();
                contentPanel.add(closed.getRootPanel());
                contentPanel.revalidate();
                contentPanel.repaint();
            }
        });
//        categories.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                ToggleButtonColors(categories);
//            }
//        });
    }

    private void ToggleButtonColors(JButton active) {
        //Set all buttons to sidebar light grey (unselected)
        for (Component component : sidebar.getComponents()) {
            if(component instanceof JButton){
                component.setBackground(new Color(200,200,200));
            }
        }

        //Set the activated button to dark grey
        active.setBackground(new Color(147,149,151));
    }

    public JPanel getRootPanel() {
        return rootPanel;
    }

}

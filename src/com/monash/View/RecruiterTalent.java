package com.monash.View;

import com.monash.model.Job;
import com.monash.model.Search.SearchResult;
import com.monash.model.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class RecruiterTalent {
    private JPanel sidebar;
    private JPanel rootPanel;
    private JPanel scrollContent;
    private JTextArea from;
    private JScrollPane scrollView;
    private JScrollPane sidebarScroll;
    private JPanel sidebarScrollContent;
    private JTextArea heading;
    private JTextArea subHeading;
    private JPanel content;
    private ArrayList<JButton> buttonList;


    public JPanel getRootPanel() {
        return rootPanel;
    }

    //Load jobs list

    public RecruiterTalent() {
        scrollView.setBorder(BorderFactory.createEmptyBorder());

        sidebarScroll.setBorder(BorderFactory.createEmptyBorder());
        sidebarScrollContent.setBorder(BorderFactory.createEmptyBorder());

        sidebarScrollContent.setLayout(new BoxLayout(sidebarScrollContent,BoxLayout.Y_AXIS));
        refreshJobButtons();

        displayMessage("Select a job to begin searching for candidates");

        scrollContent.add(Box.createGlue());
    }

    private void displayMessage(String message) {
        scrollContent.removeAll();
        scrollContent.setLayout(new GridBagLayout());
        JTextArea selectJobText = new JTextArea();
        selectJobText.setText(message);
        selectJobText.setFont(new Font("Arial", Font.PLAIN, 16));
        selectJobText.setBackground(new Color(227,229,229));
        selectJobText.setForeground(new Color(65,63,64));
        selectJobText.setFocusable(false);
        selectJobText.setEditable(false);
        scrollContent.add(selectJobText);

        scrollContent.revalidate();
        scrollContent.repaint();
    }


    public void refreshJobButtons() {
        buttonList = new ArrayList<>();
        sidebarScrollContent.removeAll();
        sidebarScrollContent.setAlignmentX(0);

        var jobs = FrameManager.getInstance().getController().getRecruiterJobList();

        ArrayList<Job> openJobs = new ArrayList<>();

        if(jobs == null){
            return;
        }

        for (Job job: jobs) {
            if(job.isActive()){
                openJobs.add(job);
            }
        }

        System.out.println("There are " + openJobs.size() + " open Jobs");

        for (int i = 0; i < openJobs.size(); i++) {
            JButton button = new JButton();
            button.setText(openJobs.get(i).getTitle());
            button.setFont(new Font("Arial", Font.PLAIN, 14));
            button.setPreferredSize(new Dimension(200,40));
            button.setMinimumSize(new Dimension(200,40));;
            button.setMaximumSize(new Dimension(200,40));
            button.setBackground(new Color(200,200,200));

            button.setBorderPainted(false);
            button.setFocusable(false);
            button.setFocusPainted(false);
            button.setRequestFocusEnabled(false);

            var job = openJobs.get(i);

            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.out.println("Show candidates for " + job.getTitle());
                    filterForTalent(job);
                    ToggleActiveButton(button);
                }
            });

            button.setHorizontalAlignment(SwingConstants.LEFT);

            sidebarScrollContent.add(button);
            buttonList.add(button);
        }



        sidebarScrollContent.add(Box.createGlue());

        sidebarScroll.getVerticalScrollBar().setUnitIncrement(20);

        sidebarScrollContent.revalidate();
        sidebarScrollContent.repaint();


    }

    private void ToggleActiveButton(JButton button) {
        //Set all buttons to sidebar light grey (unselected)
        for (Component component : sidebarScrollContent.getComponents()) {
            if(component instanceof JButton){
                component.setBackground(new Color(200,200,200));
            }
        }

        //Set the activated button to dark grey
        button.setBackground(new Color(147,149,151));
    }

    private void filterForTalent(Job openJob) {
        scrollContent.removeAll();
        scrollContent.setLayout(new BoxLayout(scrollContent, BoxLayout.PAGE_AXIS));

        System.out.println("Initiate search for " + openJob.getTitle() + " with ID " + openJob.getJobId());

        var results = FrameManager.getInstance().getController().doTalentMatch(openJob);

        if(results.size() != 0){
            for (SearchResult<User> result : results) {
                SeekerPanel panel = new SeekerPanel(result.getEntity(),result.getResult(),openJob);
                scrollContent.add(panel.getRootPanel());
                scrollContent.add(Box.createRigidArea(new Dimension(-1, 10)));

            }

            scrollContent.add(Box.createGlue());

            scrollView.getVerticalScrollBar().setUnitIncrement(20);

            scrollView.revalidate();
            scrollView.repaint();

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    scrollView.getVerticalScrollBar().setValue(0);
                }
            });
        }
        else{
            displayMessage("There are no candidates matching this job");
        }




    }

}

package com.monash.View;

import com.monash.App.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Generates the static outer frame used for GUI display to seekers.
 * @author Lea Walton
 */
public class SeekerStaticFrame {
    //Card names
    final static String SEARCH = "Job Search";
    final static String PROFILE = "Profile";
    final static String MAIL = "Mail";
    final static String PASS = "Password";

    private JPanel contentPanel;
    private JButton jobSearch;
    private JButton profile;
    private JButton mail;
    private JPanel rootPanel;
    private JLabel logoJSS;
    private JPanel mailLine;
    private JPanel profileLine;
    private JPanel jobSearchLine;
    private JButton signout;
    private JButton settings;
    private JTextArea user;
    private JPanel settingsLine;

    /**
     * Default constructor
     */
    public SeekerStaticFrame() {
        try {
            FrameManager.getInstance().getController().loadSeekData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        user.setText("User: " + FrameManager.getInstance().getController().getUser().getFirstName() + " " + FrameManager.getInstance().getController().getUser().getLastName());

        ImageIcon jssLogo = new ImageIcon("resources/Jss_logo_short.png");
        this.logoJSS.setIcon(jssLogo);

        profileLine.setVisible(false);
        mailLine.setVisible(false);
        settingsLine.setVisible(false);

        SeekerSearch ss = new SeekerSearch();
        SeekerProfileStaticFrame sp = new SeekerProfileStaticFrame();
        SeekerMail sm = null;
        try {
            sm = new SeekerMail();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Password pw = new Password();

        contentPanel.add(ss.getRootPanel(), SEARCH);
        contentPanel.add(sp.getRootPanel(), PROFILE);
        contentPanel.add(sm.getRootPanel(), MAIL);
        contentPanel.add(pw.getRootPanel(), PASS);

        CardLayout cl = (CardLayout)(contentPanel.getLayout());


        jobSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jobSearchLine.setVisible(true);
                profileLine.setVisible(false);
                mailLine.setVisible(false);
                settingsLine.setVisible(false);

                cl.show(contentPanel,SEARCH);
            }
        });
        profile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jobSearchLine.setVisible(false);
                profileLine.setVisible(true);
                mailLine.setVisible(false);
                settingsLine.setVisible(false);

                cl.show(contentPanel,PROFILE);
            }
        });
        mail.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jobSearchLine.setVisible(false);
                profileLine.setVisible(false);
                mailLine.setVisible(true);
                settingsLine.setVisible(false);

                cl.show(contentPanel,MAIL);
            }
        });
        signout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrameManager.getInstance().getController().setUser(null);
                MainLogin login = new MainLogin();
                FrameManager.getInstance().getMainFrame().setContentPane(login.getRootPanel());
                FrameManager.getInstance().getMainFrame().revalidate();
                FrameManager.getInstance().getMainFrame().repaint();
                FrameManager.getInstance().getController().clearData();
            }
        });
        settings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settingsLine.setVisible(true);
                jobSearchLine.setVisible(false);
                profileLine.setVisible(false);
                mailLine.setVisible(false);
                cl.show(contentPanel, PASS);
            }
        });
    }

    /**
     * Gets the root JPanel that holds the display for the static frame.
     * @return  the root JPanel
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }
}

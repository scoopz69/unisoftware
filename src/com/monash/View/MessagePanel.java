package com.monash.View;

import com.monash.model.Message;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MessagePanel extends ExpandablePanel {
    private Message message;
    private JPanel indicator;
    private ImageIcon openArrow;
    private ImageIcon closeArrow;
    private JLabel arrow;
    private Font font = new Font("Arial", Font.PLAIN, 16);

    public MessagePanel(Message message) {
        this.message = message;

        topPanel.setLayout(new GridBagLayout());
        topPanel.setBorder(BorderFactory.createEmptyBorder());

        populateTopPanel();
        populateBottomPanel();

    }

    private void populateBottomPanel() {
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.PAGE_AXIS));
        //CREATE BOTTOM PANEL

        bottomPanel.setBackground(Color.red);

        JTextArea messagePanel = new JTextArea(message.getMessageContent());
        messagePanel.setLineWrap(true);
        messagePanel.setMargin(new Insets(10, 15, 10, 15));
        messagePanel.setFont(font);
        messagePanel.setEditable(false);
        messagePanel.setFocusable(false);
        addListeners(messagePanel);
        //messagePanel.setPreferredSize(new Dimension(Short.MAX_VALUE,100));
        //messagePanel.setBackground(Color.blue);
        //bottomPanel.setMaximumSize(new Dimension(Short.MAX_VALUE,50));
        bottomPanel.add(messagePanel);

    }

    private void populateTopPanel() {
        GridBagLayout gbl = new GridBagLayout();
        topPanel.setLayout(gbl);

        gbl.columnWeights = new double[] {0.01,0.29,0.6,0.05,0.05};

        GridBagConstraints c = new GridBagConstraints();

        //Create read indicator
        indicator = new JPanel();
        if (!message.getRead()) {
            indicator.setBackground(Color.BLACK);
        } else {
            indicator.setBackground(Color.WHITE);
        }
        addListeners(indicator);

        c.fill = GridBagConstraints.HORIZONTAL;
        topPanel.add(indicator,c);

        //Create name field
        JTextArea recruiterName = new JTextArea();
        recruiterName.setText(message.getSender());
        //recruiterName.setBackground(Color.magenta);
        recruiterName.setFont(font);
        recruiterName.setMargin(new Insets(0, 25, 0, 0));
        recruiterName.setEditable(false);
        recruiterName.setFocusable(false);
        recruiterName.setPreferredSize(new Dimension(400,19));
        c.fill = GridBagConstraints.HORIZONTAL;
        topPanel.add(recruiterName, c);
        addListeners(recruiterName);

        c = new GridBagConstraints();

        //Create role field
        JTextArea role = new JTextArea();
        role.setText(message.getRoleName() == null ? "" : message.getRoleName());
        //role.setBackground(Color.green);
        role.setFont(font);
        role.setEditable(false);
        role.setFocusable(false);
        role.setPreferredSize(new Dimension(300,19));
        c.fill = GridBagConstraints.HORIZONTAL;
        topPanel.add(role, c);
        addListeners(role);

        c = new GridBagConstraints();

        //Create date field
        JTextArea date = new JTextArea();
        //date.setBackground(Color.cyan);
        date.setText(message.getDate());
        date.setFont(font);
        date.setEditable(false);
        date.setFocusable(false);
        date.setPreferredSize(new Dimension(50,19));
        topPanel.add(date, c);
        addListeners(date);

        //Create open/close button
        openArrow = new ImageIcon("resources/arrow_down.png");
        Image openImg = openArrow.getImage();
        Image openImgScld = openImg.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
        openArrow = new ImageIcon(openImgScld);

        closeArrow = new ImageIcon("resources/arrow_up.png");
        Image closeImg = closeArrow.getImage();
        Image closeImgScld = closeImg.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
        closeArrow = new ImageIcon(closeImgScld);

        arrow = new JLabel();
        //arrow.setPreferredSize(new Dimension(50,30));
        //arrow.setBackground(Color.orange);
        arrow.setIcon(openArrow);
        topPanel.add(arrow, c);

        addListeners(arrow);
    }

    @Override
    protected void ToggleExpand() {
        if (!expanded) {
            message.setRead(true);
            indicator.setBackground(Color.WHITE);

            arrow.setIcon(closeArrow);
        } else {
            arrow.setIcon(openArrow);
        }

        super.ToggleExpand();
    }
}

package com.monash.View;

import javax.swing.*;

public class SeekerSkills {
    private JPanel rootPanel;
    private JTextArea heading;
    private JTextArea subHeading;
    private JScrollPane scrollView;
    private JPanel scrollContent;
    private JTextPane noSkillsTextArea;
    private JTextArea addCategory;
    private JButton addSkill;
    private JButton saveChangesButton;
    private JButton revertChanges;
    private JTextArea message;

    /**
     * Gets the root JPanel to display the screen.
     * @return root JPanel.
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }

}

package com.monash.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ExpandablePanel {
    private JPanel rootPanel;
    protected JPanel topPanel;
    private JPanel splitter;
    protected JPanel bottomPanel;
    protected Boolean expanded;

    public ExpandablePanel() {

        topPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                ToggleExpand();

            }
        });

        //Close panel for initial display
        bottomPanel.setEnabled(false);
        bottomPanel.setVisible(false);
        splitter.setVisible(false);
        splitter.setEnabled(false);
        expanded = false;
    }

    protected void addListeners(JComponent component){
        component.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                ToggleExpand();
            }
        });
    }

    protected void ToggleExpand() {
        if (expanded) {
            //OPEN TO CLOSED
            bottomPanel.setEnabled(false);
            bottomPanel.setVisible(false);
            splitter.setVisible(false);
            splitter.setEnabled(false);
            rootPanel.setMaximumSize(new Dimension(Short.MAX_VALUE,topPanel.getMaximumSize().height));
            rootPanel.setPreferredSize(topPanel.getPreferredSize());
        } else {
            //CLOSED TO OPEN
            bottomPanel.setEnabled(true);
            bottomPanel.setVisible(true);
            splitter.setVisible(true);
            splitter.setEnabled(true);
            rootPanel.setMaximumSize(new Dimension(Short.MAX_VALUE,150));
            rootPanel.setPreferredSize(new Dimension( rootPanel.getPreferredSize().width,
                    topPanel.getPreferredSize().height + splitter.getPreferredSize().height + bottomPanel.getPreferredSize().height));
        }

        expanded = !expanded;


        rootPanel.revalidate();
        rootPanel.repaint();

        //TODO: check if this is really required
        rootPanel.getParent().revalidate();
        rootPanel.getParent().repaint();

    }

    public JPanel getRootPanel() {
        return this.rootPanel;
    }

    public void setRootPanel(JPanel rootPanel) {
        this.rootPanel = rootPanel;
    }
}

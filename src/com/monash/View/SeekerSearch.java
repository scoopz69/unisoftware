package com.monash.View;

import com.monash.controller.Controller;
import com.monash.model.Job;
import com.monash.model.Search.SearchResult;
import com.monash.model.Util;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import javax.swing.text.TableView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.util.*;
import java.util.List;

public class SeekerSearch {
    private enum MsgType {
        NONE(200, 200, 200),
        ERROR(255, 0, 0),
        WARNING(255, 165, 0),
        INFORMATION(52,107,187);

        private final Color color;

        private MsgType(int r, int g, int b) {
            this.color = new Color(r, g, b);
        }

        public Color getColor() {
            return this.color;
        }
    };

    //Names for cards
    final static String RESULTS = "Results";
    final static String BLANK = "Blank";

    private JFormattedTextField inputMin;
    private JTextField inputWhat;
    private JButton search;
    private JFormattedTextField inputMax;
    private JPanel rootPanel;
    private JPanel resultPanel;
    private JTextPane message;
    private JList<String> lbCategory;
    private JScrollPane spCategory;
    private JTextArea errorMsg;
    private JScrollPane resultsPane;
    private JTable resultsTable;

    private CardLayout subPageLayout;

    public SeekerSearch() {
        //Setting formatting type for Formatted Field (salary input) because I cant figure out how to do it in designer window.
        NumberFormat nf = NumberFormat.getIntegerInstance(); // Specify specific format here.
        NumberFormatter nff = new NumberFormatter(nf);
        DefaultFormatterFactory factory = new DefaultFormatterFactory(nff);
        inputMin.setFormatterFactory(factory);
        inputMax.setFormatterFactory(factory);

        //Get page layout ref so other cards can be added once instantiated
        subPageLayout = (CardLayout)(resultPanel.getLayout());

        //Set up the category list box
        setupCategoryLB();

        //Set up the results table
        initialiseResultsTable();

        search.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(validateSearchParameters()) {
                    message(MsgType.INFORMATION, "Searching for jobs...");
                    doSearch();
                }
            }
        });
    }

    /**
     * Validate the search parameters
     *
     * @return True if valid, false otherwise
     */
    private boolean validateSearchParameters() {
        boolean result = true;
        int payFrom = 0;
        int payTo = 0;
        
        if (inputMin.getText().isEmpty() || inputMin.getText().isBlank())
            inputMin.setText("0");

        if (inputMax.getText().isEmpty() || inputMax.getText().isBlank())
            inputMax.setText("0");
        
        try {
            payFrom = Integer.parseInt(Util.stripPunctuation(inputMin.getText()));
            payTo = Integer.parseInt(Util.stripPunctuation(inputMax.getText()));

            if (payTo < payFrom) {
                message(MsgType.ERROR, "Pay From must be less than Pay To. Please re-enter.");
                result = false;
            }
        } catch (NumberFormatException e) {
            message(MsgType.ERROR, "Pay fields must be numeric. Please re-enter");
            result = false;
        }

        return result;
    }

    /**
     * Display a message
     *
     * @param type    Message type
     * @param message Message text
     */
    private void message(MsgType type, String message) {
        errorMsg.setForeground(type.getColor());
        errorMsg.setText(message);
    }

    /**
     * Initiate the job search
     */
    private void doSearch() {
        if (validateSearchParameters()) {
            message(MsgType.INFORMATION, "Searching...");

            List<String> selectedCats = lbCategory.getSelectedValuesList();
            ArrayList<String> categories = new ArrayList<String>(selectedCats);

            ArrayList<SearchResult<Job>> results = new ArrayList<SearchResult<Job>>();

            int payMin = Integer.parseInt(Util.stripPunctuation(inputMin.getText()));
            int payMax = Integer.parseInt(Util.stripPunctuation(inputMax.getText()));

            results = getController().doJobSearch(categories, inputWhat.getText(), " ", payMin, payMax);
            message(MsgType.INFORMATION, results.size() + (results.size() == 1 ? " job" : " jobs") + " found.");

            if (!results.isEmpty()) {
                populateResultsTable(results);
                showResults();
            }
            else {
                showNoResults();
            }
        }
    }

    public JPanel getRootPanel() { return rootPanel; }

    public void setRootPanel(JPanel rootPanel) { this.rootPanel = rootPanel; }

    /**
     * Initialise the results table
     */
    private void initialiseResultsTable() {
        lbCategory.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                if (e.getClickCount() == 2) {
                    // TODO: Add code to display job details screen
                }
            }
        });

        DefaultTableModel model = new DefaultTableModel();
        // model.setRowCount(0);

        model.addColumn("Job ID");
        model.addColumn("Company");
        model.addColumn("Job Title");
        model.addColumn("Location");
        model.addColumn("Summary");
        model.addColumn("Min. Salary");
        model.addColumn("Max. Salary");
        model.addColumn("Match %");
        model.addColumn("");

        resultsTable.setModel(model);
        resultsTable.setVisible(true);
    }

    /**
     * Populate the seeker search results table
     * @param results Job search results
     */
    private void populateResultsTable(ArrayList<SearchResult<Job>> results) {
        DefaultTableModel model = (DefaultTableModel) resultsTable.getModel();
        model.setRowCount(0);

        for (SearchResult<Job> result : results) {
            String[] row = {String.valueOf(result.getEntity().getJobId()),
                            result.getEntity().getCompany(),
                            result.getEntity().getTitle(),
                            result.getEntity().getLocation(),
                            result.getEntity().getSummary(),
                            String.valueOf(result.getEntity().getPayLow()),
                            String.valueOf(result.getEntity().getPayHigh()),
                            String.valueOf(result.getResult())};

            model.addRow(row);
        }
     }

    /**
     * Show the results card
     */
    private void showResults() {
        CardLayout resultPanelLayout = (CardLayout)resultPanel.getLayout();
        resultPanelLayout.show(resultPanel, "results");
    }

    /**
     * Show the no results card
     */
    private void showNoResults() {
        CardLayout resultPanelLayout = (CardLayout)resultPanel.getLayout();
        resultPanelLayout.show(resultPanel, "noResults");
    }

    /**
     * Get a controller instance
     *
      * @return Controller instance
     */
    public Controller getController() {
        return FrameManager.getInstance().getController();
    }

    /**
     * Populate the category dropdown
     */
    public void setupCategoryLB() {
        lbCategory.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                if (e.getClickCount() == 2) {
                    int[] selected = {};
                    lbCategory.setSelectedIndices(selected);
                }
            }
        });

        try {
            HashMap<Integer, String> catMap = getController().getCategories();
            String[] catArray = catMap.values().toArray(new String[0]);
            List<String> catList = new ArrayList<String>();
            catList = Arrays.asList(catArray);
            Collections.sort(catList);

            DefaultListModel<String> listModel = new DefaultListModel<String>();
            lbCategory.setModel(listModel);

            for (int i = 0; i < catList.size(); i++)
                listModel.addElement(catList.get(i));
        }
        catch (Exception e) {
            message(MsgType.ERROR, "No categories available");
        }
    }
}

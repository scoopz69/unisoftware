package com.monash.View;

import javax.swing.*;

public class SeekerContactDetails {

    private boolean showOpen;

    private JPanel rootPanel;
    private JTextArea contactDetails;
    private JTextArea keepYourDetailsUpTextArea;
    private JPanel detailsForm;
    private JComboBox salutation;
    private JTextField firstName;
    private JLabel title;
    private JLabel name;
    private JTextField lastNameTextField;
    private JTextField mobilePhone;
    private JTextField homePhone;
    private JTextField textField3;
    private JTextField buildingNumber;
    private JTextField streetNumber;
    private JTextField streetName;
    private JTextField suburb;
    private JTextField state;
    private JTextField postcode;
    private JTextPane addressTextPane;
    private JButton saveChanges;
    private JTextArea changesSavedSuccessfullyTextArea;


    public SeekerContactDetails(){


    }



    /**
     * Gets the root JPanel to display the screen.
     * @return root JPanel.
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }


}

package com.monash.View;

import com.monash.model.Message;
import com.monash.model.User;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;

public class SeekerMail {
    private JPanel rootPanel;
    private JScrollPane scrollWindow;
    private JPanel buttonPanel;
    private JPanel mailHeader;
    private JTextArea from;
    private JTextArea role;
    private JTextArea receivedDate;
    private JTextArea headerMsg;
    private JPanel scrollViewArea;

    public JPanel getRootPanel() {
        return rootPanel;
    }

    public SeekerMail() throws SQLException {

        refreshMail();
        scrollViewArea.setLayout(new BoxLayout(scrollViewArea,BoxLayout.Y_AXIS));

        //System.out.println("scroll view layout: " + scrollViewArea.getLayout().toString());

        scrollWindow.setBorder(BorderFactory.createEmptyBorder());

    }

    private void refreshMail() throws SQLException {
        scrollViewArea.removeAll();

        User user = FrameManager.getInstance().getController().getUser();
        var messages = FrameManager.getInstance().getController().getMessageList(user);

        System.out.println("Loading messages for user: " + user.getId());

        for(int i = 0; i < messages.size(); i++){
            var panel = new MessagePanel(messages.get(i));
            scrollViewArea.add(panel.getRootPanel());
            scrollViewArea.add(Box.createRigidArea(new Dimension(-1, 15)));
        }

        scrollViewArea.add(Box.createGlue());


        //Increase Scroll speed
        scrollWindow.getVerticalScrollBar().setUnitIncrement(20);
        scrollViewArea.revalidate();
        scrollViewArea.repaint();

        //Refresh scroll position to top of
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                scrollWindow.getVerticalScrollBar().setValue(0);
            }
        });
    }

}

package com.monash.View;

import javax.swing.*;
import java.awt.*;

public class InputSlot {
    private JPanel rootPanel;
    private JTextField input;
    private JButton delete;

    public InputSlot(){
        ImageIcon icon = new ImageIcon("resources/delete_grey.png");
        Image iconImg = icon.getImage();
        Image imgScld = iconImg.getScaledInstance(20,20,Image.SCALE_SMOOTH);
        ImageIcon iconScld= new ImageIcon(imgScld);
        delete.setIcon(iconScld);

    }

    public JTextField getInput() {
        return input;
    }

    public void setInput(JTextField input) {
        this.input = input;
    }

    public JButton getDelete() {
        return delete;
    }

    public void setDelete(JButton delete) {
        this.delete = delete;
    }

    public JPanel getRootPanel() {
        return rootPanel;
    }

    public void setRootPanel(JPanel rootPanel) {
        this.rootPanel = rootPanel;
    }
}

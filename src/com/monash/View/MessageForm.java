package com.monash.View;

import com.monash.View.FrameManager;
import com.monash.model.Job;
import com.monash.model.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Pop up window to message a job seeker
 */
public class MessageForm {
    private JFrame frame;
    private JDialog dialog;
    private JLabel candidate;
    private JLabel role;
    private JTextArea messageField;
    private JButton send;
    private JButton cancel;

    /**
     * Non-default constructor to display popup message window for given user and job
     * @param frame
     * @param user
     * @param job
     */
    public MessageForm(JFrame frame, User user, Job job){
        this.frame = frame;

        dialog = new JDialog(frame, Dialog.ModalityType.APPLICATION_MODAL);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setLocationRelativeTo(frame);
        dialog.setModal(true);
        dialog.setSize(800, 500);

        Font mainFont = new Font("Arial", Font.PLAIN,16);


        JPanel mainPanel = new JPanel();
        mainPanel.setBackground(new Color(52,107,187));
        mainPanel.setLayout(new GridBagLayout());

        dialog.add(mainPanel);

        JPanel formPanel = new JPanel();
        formPanel.setBackground(new Color(227,229,229));
        formPanel.setPreferredSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
        formPanel.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(10,10,10,10);
        c.weightx = 1;
        c.weighty = 1;
        mainPanel.add(formPanel,c);

        JTextArea heading = new JTextArea("Send Message");
        heading.setFocusable(false);
        heading.setEditable(false);
        heading.setForeground(new Color(52,107,187));
        heading.setBackground(new Color(227,229,229));
        heading.setFont(new Font("Arial", Font.PLAIN, 20));
        c = new GridBagConstraints();
        c.gridy = 0;
        c.gridx = 0;
        c.weightx = 1;
        c.gridwidth = 2;
        c.insets = new Insets(15,10,10,10);
        formPanel.add(heading,c);

        candidate = new JLabel("To: " + user.getFirstName() + " " + user.getLastName());
        candidate.setFocusable(false);
        candidate.setFont(mainFont);
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridy = 1;
        c.gridx = 0;
        c.gridwidth = 2;
        c.insets = new Insets(10,10,5,10);
        formPanel.add(candidate,c);

        role = new JLabel("Role: " + job.getTitle());
        role.setFocusable(false);
        role.setFont(mainFont);
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridy = 2;
        c.gridx = 0;
        c.gridwidth = 2;
        c.insets = new Insets(5,10,10,10);
        formPanel.add(role,c);

        messageField = new JTextArea();
        messageField.setLineWrap(true);
        messageField.setWrapStyleWord(true);
        messageField.setMinimumSize(new Dimension(500,350));
        messageField.setPreferredSize(new Dimension(500,Short.MAX_VALUE));
        messageField.setFont(mainFont);
        messageField.setMargin(new Insets(5,5,5,5));

        String defaultMsg = "";
        defaultMsg += (user.getFirstName() != null) ? "Hi " + user.getFirstName() + "\n\n" : "Hi + \n\n";
        defaultMsg += "I think you're well suited for this role. If you would like to discuss further please contact me at ";
        defaultMsg += FrameManager.getInstance().getController().getUser().getEmail() + ". \n\n";
        defaultMsg += "Kind Regards,\n\n";
        defaultMsg += FrameManager.getInstance().getController().getUser().getFirstName() + " " + FrameManager.getInstance().getController().getUser().getLastName();

        messageField.setText(defaultMsg);

        c.gridy = 3;
        c.gridheight = 3;
        c.gridwidth = 2;
        c.weighty = 1;
        formPanel.add(messageField,c);

        send = new JButton("Send");
        send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrameManager.getInstance().getController().addMessage("",job.getTitle(),messageField.getText(),false, job.getRecruiterId(), user.getId());
            }
        });
        send.setPreferredSize(new Dimension(250,30));
        send.setBackground(new Color(57,107,187));
        send.setForeground(Color.WHITE);
        send.setFont(mainFont);
        send.setFocusable(false);
        c = new GridBagConstraints();
        c.weightx = 0.5;
        c.gridy = 7;
        c.gridx = 1;
        c.insets = new Insets(10,0,10,10);
        formPanel.add(send,c);

        cancel = new JButton("Cancel");
        cancel.setPreferredSize(new Dimension(250,30));
        cancel.setBackground(new Color(147,149,151));
        cancel.setForeground(Color.WHITE);
        cancel.setFont(mainFont);
        cancel.setFocusable(false);
        c = new GridBagConstraints();
        c.weightx = 0.5;
        c.gridy = 7;
        c.gridx = 0;
        c.insets = new Insets(10,0,10,10);
        formPanel.add(cancel,c);

        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.dispose();
            }
        });


        dialog.revalidate();
        dialog.repaint();

        dialog.setVisible(true);

    }
}

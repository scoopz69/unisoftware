package com.monash.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Generates the static outer frame used for GUI display to recruiters
 * @author Lea Walton
 */
public class RecruiterStaticFrame {
    final static String JOBS = "Jobs";
    final static String PROFILE = "Profile";
    final static String TALENT = "Talent";
    final static String PASS = "Settings";

    private JButton jobs;
    private JButton talent;
    private JButton profile;
    private JLabel logoJSS;
    private JPanel talentLine;
    private JPanel jobLine;
    private JPanel profileLine;
    private JPanel contentPanel;
    private JPanel rootPanel;
    private JButton settings;
    private JButton signout;
    private JPanel settingsLine;
    private JTextArea user;

    /**
     * Default constructor
     */
    public RecruiterStaticFrame() {
        FrameManager.getInstance().getController().loadRecData(FrameManager.getInstance().getController().getSessionUserId());

        user.setText("User: " + FrameManager.getInstance().getController().getUser().getFirstName() + " " + FrameManager.getInstance().getController().getUser().getLastName());

        ImageIcon jssLogo = new ImageIcon("resources/Jss_logo_short.png");
        this.logoJSS.setIcon(jssLogo);
        profileLine.setVisible(false);
        talentLine.setVisible(false);
        settingsLine.setVisible(false);

        //TODO: update with recruiter pages
        RecruiterJobs rj = new RecruiterJobs();
        SeekerProfile sp = new SeekerProfile();
        RecruiterTalent rt = new RecruiterTalent();
        Password pw = new Password();

        contentPanel.add(rj.getRootPanel(), JOBS);
        contentPanel.add(sp.getRootPanel(), PROFILE);
        contentPanel.add(rt.getRootPanel(), TALENT);
        contentPanel.add(pw.getRootPanel(), PASS);

        CardLayout cl = (CardLayout)(contentPanel.getLayout());

        jobs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jobLine.setVisible(true);
                profileLine.setVisible(false);
                talentLine.setVisible(false);
                settingsLine.setVisible(false);

                cl.show(contentPanel, JOBS);
            }
        });
        profile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jobLine.setVisible(false);
                profileLine.setVisible(true);
                talentLine.setVisible(false);
                settingsLine.setVisible(false);

                cl.show(contentPanel,PROFILE);
            }
        });
        talent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jobLine.setVisible(false);
                profileLine.setVisible(false);
                talentLine.setVisible(true);
                settingsLine.setVisible(false);
                rt.refreshJobButtons();

                cl.show(contentPanel,TALENT);
            }
        });

        signout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrameManager.getInstance().getController().setUser(null);
                MainLogin login = new MainLogin();
                FrameManager.getInstance().getMainFrame().setContentPane(login.getRootPanel());
                FrameManager.getInstance().getMainFrame().revalidate();
                FrameManager.getInstance().getMainFrame().repaint();
                FrameManager.getInstance().getController().clearData();
            }
        });

        settings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settingsLine.setVisible(true);
                talentLine.setVisible(false);
                profileLine.setVisible(false);
                jobLine.setVisible(false);
                cl.show(contentPanel, PASS);
            }
        });

        rootPanel.revalidate();
        rootPanel.repaint();
    }

    /**
     * Gets the root JPanel that holds the display for the static frame.
     * @return  the root JPanel
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }
}

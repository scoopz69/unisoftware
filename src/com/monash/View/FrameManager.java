package com.monash.View;

import com.monash.controller.Controller;
import com.monash.model.User;

import javax.swing.*;
import java.sql.SQLException;

/**
 * Frame Manager is a singleton class that controls the main JFrame of the application.
 *
 * @author Lea Walton
 */
public class FrameManager {
    private static FrameManager instance;
    private JFrame mainFrame;
    private Controller controller;

    /**
     * Default Constructor
     */
    public FrameManager() {
        //Singleton initialisation
        //Not thread safe, but application is not multi-threaded
        if (instance == null) {
            instance = this;
        } else {
            System.out.println("Error: multiple copies of FrameManager");
        }

        mainFrame = new JFrame("Monash JSS");
        mainFrame.setSize(1920, 1080);

        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * Gets the singleton instance of FrameManager.
     *
     * @return the FrameManager instance.
     */
    public static FrameManager getInstance() {
        return instance;
    }

    /**
     * Returns the main JFrame that the application screens are displayed in.
     *
     * @return the main JFrame window.
     */
    public JFrame getMainFrame() {
        return mainFrame;
    }

    /**
     * Set the main JFrame to display in the application window.
     *
     * @param mainFrame
     */
    public void setMainFrame(JFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    /**
     * Gets the controller for use by GUI classes displayed in main JFrame.
     *
     * @return the controller.
     */
    public Controller getController() {
        return controller;
    }

    /**
     * Sets the controller to be used by GUI classes displayed in main JFrame.
     *
     * @param controller
     */
    public void setController(Controller controller) {
        this.controller = controller;
    }

    /**
     * Loads the appropriate logged in menu depending on the type of user.
     * @param user
     */
    public void loadLoggedInFrames(User user) {
        if(user.getRecruiter()){
            RecruiterStaticFrame recruiter = new RecruiterStaticFrame();
            FrameManager.getInstance().getMainFrame().setContentPane(recruiter.getRootPanel());

            FrameManager.getInstance().getMainFrame().revalidate();
            FrameManager.getInstance().getMainFrame().repaint();
        }
        else{
            SeekerStaticFrame seeker = new SeekerStaticFrame();
            FrameManager.getInstance().getMainFrame().setContentPane(seeker.getRootPanel());
        }
    }

    public void swapIntoPanel(JPanel parent, JPanel substitute){
        parent.removeAll();
        parent.add(substitute);
        parent.revalidate();
        parent.repaint();
    }
}

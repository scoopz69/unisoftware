package com.monash.View;

import javax.swing.*;

public class SeekerAppliedJobs {
    private JTextField itWorksTextField;
    private JPanel rootPanel;

    /**
     * Gets the root JPanel to display the screen.
     * @return root JPanel.
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }

}

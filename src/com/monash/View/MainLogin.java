package com.monash.View;

import com.monash.controller.Controller;
import com.monash.model.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;

/**
 * MainLogin provides user with visual display of login capability.
 * @author Lea Walton
 */
public class MainLogin {
    private Boolean devModeLogin = true;    //TODO: set to false for production

    private JLabel monashLogo;
    private JPanel rootPanel;
    private JLabel jssLogo;
    private JPanel loginWindow;
    private JTextArea jobSeekerSystemTextArea;
    private JTextArea errorMsg;
    private JTextField username;
    private JPasswordField password;
    private JButton signInButton;
    private JButton register;
    private JLabel usernameLabel;
    private JLabel pwdLabel;
    private JTextArea registerTxtPrompt;

    /**
     * Default Constructor
     */
    public MainLogin() {
        ImageIcon monashLogo = new ImageIcon("resources/monashlogo_white_300px.png");
        this.monashLogo.setIcon(monashLogo);

        ImageIcon jssLogo = new ImageIcon("resources/jss_logo_long.png");
        this.jssLogo.setIcon(jssLogo);;

        signInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                signInButton.setEnabled(false);

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        TryAttemptSignIn();
                    }
                };

                //Display status while attempting sign in
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        errorMsg.setForeground(new Color(52,107,187));
                        errorMsg.setText("Signing in...");
                        errorMsg.setEnabled(true);
                    }
                });

                Thread thread = new Thread(r);
                thread.start();

                System.out.println("Sign In button pressed");
            }
        });
        register.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                var registerFrame = new Registration();

                FrameManager.getInstance().getMainFrame().setContentPane(registerFrame.getRootPanel());
            }
        });
    }

    /**
     * Method to check if the data put into username and password fields
     * matches with a current registered user and allows sign-in to proceed.
     */
    private void TryAttemptSignIn() {
        //If dev mode enabled blank input will automatically sign in as seek@jss.com
        //TODO: disable this before production
        if(devModeLogin && username.getText().equals("a") && String.valueOf(password.getPassword()).isBlank()){
            AdminStaticFrame admin = new AdminStaticFrame();
            FrameManager.getInstance().getMainFrame().setContentPane(admin.getRootPanel());

            FrameManager.getInstance().getMainFrame().revalidate();
            FrameManager.getInstance().getMainFrame().repaint();
        }

        if(devModeLogin && username.getText().isBlank() && String.valueOf(password.getPassword()).isBlank()){
            User loginUser = FrameManager.getInstance().getController().getDb().dbCheckUserLogin("seek@jss.com", "123456");
            FrameManager.getInstance().getController().setUser(loginUser);
            FrameManager.getInstance().loadLoggedInFrames(loginUser);
        }

        if(devModeLogin && username.getText().equals("r") && String.valueOf(password.getPassword()).isBlank()){
            User loginUser = FrameManager.getInstance().getController().getDb().dbCheckUserLogin("jonsnow@jss.com", "123456");
            FrameManager.getInstance().getController().setUser(loginUser);
            FrameManager.getInstance().loadLoggedInFrames(loginUser);
        }

        String username = this.username.getText();
        String password = String.valueOf(this.password.getPassword());


        //TODO add controller connection
        User loginUser = FrameManager.getInstance().getController().getDb().dbCheckUserLogin(username, password);

        //Check text of username, this is where we would check if username is valid and a seeker or recruiter.
        if(loginUser != null){

            FrameManager.getInstance().getController().setUser(loginUser);
            FrameManager.getInstance().loadLoggedInFrames(loginUser);

        }
        else{
            signInButton.setEnabled(true);

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    errorMsg.setForeground(Color.RED);
                    errorMsg.setText("Login failed, please check username and password");
                    errorMsg.setEnabled(true);
                }
            });

        }
    }

    /**
     * Gets the root JPanel to display the login screen.
     * @return root JPanel.
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }

}

package com.monash.View;

import com.monash.model.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.SQLException;

/**
 * Allows user to enter their details for account creation.
 * @author Lea Walton
 */

public class RegistrationDetails {

    private JLabel monashLogo;
    private JLabel jssLogo;
    private JPanel loginWindow;
    private JButton completeButton;
    private JPanel rootPanel;
    private JTextField firstName;
    private JTextField lastName;
    private JPasswordField pwd;
    private JPasswordField pwdConfirm;
    private JTextField email;
    private JTextArea errFirst;
    private JTextArea errLast;
    private JTextArea errEmail;
    private JTextArea errPwd;
    private JTextArea errPwdMatch;
    private JTextArea heading;
    private JPanel loginFields;
    private JButton back;
    private JButton next;
    private JTextPane pageNo;
    private JButton cancel;
    private User user;
    private ActionListener completeAction;
    /**
     * Default constructor
     */
    public RegistrationDetails() {
        ImageIcon monashLogo = new ImageIcon("resources/monashlogo_white_300px.png");
        this.monashLogo.setIcon(monashLogo);

        ImageIcon jssLogo = new ImageIcon("resources/Jss_logo_long.png");
        this.jssLogo.setIcon(jssLogo);

        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0,30,0,30);

        errEmail.setEnabled(false);
        errFirst.setEnabled(false);
        errLast.setEnabled(false);
        errPwd.setEnabled(false);
        errPwdMatch.setEnabled(false);

        completeAction = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(user != null) {
                        RegisterNewUser(user);
                }
            }
        };

        completeButton.addActionListener(completeAction);

        firstName.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                errorDisplayFirstName();
            }
        });
        lastName.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                errorDisplayLastName();
            }
        });
        email.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                errorDisplayEmail();

            }
        });
        pwd.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                errorDisplayPassword();
            }
        });
        pwdConfirm.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                String password1 = String.valueOf(pwd.getPassword());
                String password2 = String.valueOf(pwdConfirm.getPassword());
                errorDisplayPasswordConfirm(password1, password2);
            }
        });

        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Registration registration = new Registration();
                FrameManager.getInstance().getMainFrame().setContentPane(registration.getRootPanel());
                FrameManager.getInstance().getMainFrame().revalidate();
                FrameManager.getInstance().getMainFrame().repaint();
            }
        });
        next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RegisterNewUser(user);
            }
        });
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Registration registration = new Registration();
                FrameManager.getInstance().getMainFrame().setContentPane(registration.getRootPanel());
                FrameManager.getInstance().getMainFrame().revalidate();
                FrameManager.getInstance().getMainFrame().repaint();
            }
        });
    }

    /**
     * Verifies input into First Name field and decides if error message needs to display.
     */
    private void errorDisplayFirstName() {
        if(FrameManager.getInstance().getController().getInputVerification().nameIsValid(firstName.getText())){
            errFirst.setEnabled(false);
        }
        else{
            errFirst.setEnabled(true);
        }
    }

    /**
     * Verifies input into Last Name field and decides if error message needs to display.
     */
    private void errorDisplayLastName() {
        if(FrameManager.getInstance().getController().getInputVerification().nameIsValid(lastName.getText())){
            errLast.setEnabled(false);
        }
        else{
            errLast.setEnabled(true);
        }
    }

    /**
     * Verifies input into Email field and decides if error message needs to display.
     */
    private void errorDisplayEmail() {
        if(FrameManager.getInstance().getController().getInputVerification().emailIsValid(email.getText())){
            if(FrameManager.getInstance().getController().getInputVerification().usernameAvailable(email.getText())){
                errEmail.setEnabled(false);
            }
            else{
                errEmail.setText("Email already in use");
                errEmail.setEnabled(true);
            }

        }
        else{
            errEmail.setText("Enter a valid email");
            errEmail.setEnabled(true);
        }
    }

    /**
     * Verifies input into Password field and decides if error message needs to display.
     */
    private void errorDisplayPassword() {
        if(FrameManager.getInstance().getController().getInputVerification().passwordIsValid(String.valueOf(pwd.getPassword()))){
            errPwd.setEnabled(false);
        }
        else{
            errPwd.setEnabled(true);
        }
    }

    /**
     * Verifies input into Password and Confirm Password fields and decides if error message needs to display.
     *
     * @param password1
     * @param password2
     */
    private void errorDisplayPasswordConfirm(String password1, String password2) {
        if(FrameManager.getInstance().getController().getInputVerification().checkPasswordsMatch(password1, password2)){
            errPwdMatch.setEnabled(false);
        }
        else{
            errPwdMatch.setEnabled(true);
        }
    }

    /**
     * Attempts to register the user details as a new user in the database.
     * @param user
     */
    private void RegisterNewUser(User user) {
        System.out.println("Details: User: "  + user.toString());

        String password1 = String.valueOf(pwd.getPassword());
        String password2 = String.valueOf(pwdConfirm.getPassword());

        //If any validation condition not met display errors and abort registering
        if( !FrameManager.getInstance().getController().getInputVerification().nameIsValid(firstName.getText()) ||
                !FrameManager.getInstance().getController().getInputVerification().nameIsValid(lastName.getText()) ||
                !FrameManager.getInstance().getController().getInputVerification().emailIsValid(email.getText()) ||
                !FrameManager.getInstance().getController().getInputVerification().usernameAvailable(email.getText()) ||
                !FrameManager.getInstance().getController().getInputVerification().passwordIsValid(String.valueOf(pwd.getPassword())) ||
                !FrameManager.getInstance().getController().getInputVerification().checkPasswordsMatch(password1,password2)) {

            errorDisplayFirstName();
            errorDisplayLastName();
            errorDisplayEmail();
            errorDisplayPassword();
            errorDisplayPasswordConfirm(password1,password2);
            return;
        }

        user.setEmail(email.getText());
        user.setFirstName(firstName.getText());
        user.setLastName(lastName.getText());
        user.setPassword(String.copyValueOf(pwd.getPassword()));

        try{
            FrameManager.getInstance().getController().registerUser(
                    user.getPassword(),user.getFirstName(),user.getLastName(),user.getEmail(),user.getRecruiter());
        }
        catch (SQLException e) {
            System.out.println("Unable to add user to db");
        }

        FrameManager.getInstance().getController().setUser(user);

        displaySuccessMessage();
    }

    /**
     * Method to remove input fields and replace with account creation confirmation message and proceed to dashboard button.
     */
    private void displaySuccessMessage() {
        heading.setText("Done!");

        loginFields.removeAll();
        JTextArea message = new JTextArea();
        message.setText("Your account has been created");
        message.setFont(new Font("Arial", Font.PLAIN, 16));
        message.setMargin(new Insets(20,0,20,0));
        message.setBackground(new Color(227,229,229));
        loginFields.add(message);
        loginFields.setPreferredSize(new Dimension(500,-1));

        //Re-use existing button. Replace listener and change text
        completeButton.removeActionListener(completeAction);
        completeButton.setText("Go to Dashboard");

        completeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrameManager.getInstance().loadLoggedInFrames(user);

            }
        });

        //Refresh panel display
        loginFields.revalidate();
        loginFields.repaint();
    }

    /**
     * Gets the root JPanel that holds the displaying registration detail screen.
     * @return  the root JPanel
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }

    /**
     * Sets the user for the registration process.
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }
}

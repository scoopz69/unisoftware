package com.monash.View;

import com.monash.model.User;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Initiates User registration process and allows for account type selection.
 * @author Lea Walton
 */
public class Registration {
    private JLabel monashLogo;
    private JLabel jssLogo;
    private JPanel loginWindow;
    private JButton nextButton;
    private JPanel rootPanel;
    private JRadioButton jobSeekerRB;
    private JRadioButton recruiterRB;
    private JButton back;
    private JButton next;
    private JTextPane pageNo;
    private JButton cancel;

    /**
     * Default constructor
     */
    public Registration() {
        ImageIcon monashLogo = new ImageIcon("resources/monashlogo_white_300px.png");
        this.monashLogo.setIcon(monashLogo);

        ImageIcon jssLogo = new ImageIcon("resources/Jss_logo_long.png");
        this.jssLogo.setIcon(jssLogo);

        ButtonGroup jobTypeGroup = new ButtonGroup();
        jobTypeGroup.add(jobSeekerRB);
        jobTypeGroup.add(recruiterRB);
        jobSeekerRB.setSelected(true);
        recruiterRB.setSelected(false);

        //NEXT BUTTON IN FORM
        nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoadRegistrationDetails();
            }
        });

        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainLogin login = new MainLogin();
                FrameManager.getInstance().getMainFrame().setContentPane(login.getRootPanel());
                FrameManager.getInstance().getMainFrame().revalidate();
                FrameManager.getInstance().getMainFrame().repaint();
            }
        });
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainLogin login = new MainLogin();
                FrameManager.getInstance().getMainFrame().setContentPane(login.getRootPanel());
                FrameManager.getInstance().getMainFrame().revalidate();
                FrameManager.getInstance().getMainFrame().repaint();
            }
        });
        next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoadRegistrationDetails();
            }
        });
    }

    /**
     * Takes the current selected account type and loads the appropriate details page.
     */
    private void LoadRegistrationDetails(){
        User user = new User();

        if (recruiterRB.isSelected()) {
            user.setRecruiter(true);
        } else {
            user.setRecruiter(false);
        }

        //All account types currently use same details entry page
        RegistrationDetails details = new RegistrationDetails();
        details.setUser(user);

        FrameManager.getInstance().getMainFrame().setContentPane(details.getRootPanel());
        FrameManager.getInstance().getMainFrame().revalidate();
        FrameManager.getInstance().getMainFrame().repaint();
    }

    /**
     * Gets the main Registration display panel.
     * @return  root panel as a JPanel.
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }
}

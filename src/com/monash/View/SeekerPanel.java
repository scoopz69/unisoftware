package com.monash.View;

import com.monash.model.Job;
import com.monash.model.Tag;
import com.monash.model.TagKey;
import com.monash.model.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Map;

    /**
     * GUI class to generate an expandable display to show Seeker profile information.
     */
    public class SeekerPanel extends ExpandablePanel {
    private User user;
    private JPanel indicator;
    private ImageIcon openArrow;
    private ImageIcon closeArrow;
    private JLabel arrow;
    private Font font = new Font("Arial", Font.PLAIN, 16);


    /**
     * Non-default constructor to generate panel display for given user and job match score
     * @param user
     * @param result
     */
    public SeekerPanel(User user, int result, Job job) {
        this.user = user;
        topPanel.setLayout(new GridBagLayout());
        topPanel.setBorder(BorderFactory.createEmptyBorder());

        populateTopPanel(user, result, job);
        populateBottomPanel(user,job);
    }

    /**
     * Populates the top panel with components and user information.
     * @param user
     * @param result
     */
    private void populateTopPanel(User user, int result, Job job) {
        GridBagLayout gbl = new GridBagLayout();
        topPanel.setLayout(gbl);

        gbl.columnWeights = new double[] {0.45,0.45,0.05,0.05};

        GridBagConstraints c = new GridBagConstraints();

        //Create name field
        JTextArea seekerName = new JTextArea();
        String name = user.getFirstName() + " " + user.getLastName();
        seekerName.setText(name);
        //seekerName.setBackground(Color.magenta);
        seekerName.setFont(font);
        seekerName.setMargin(new Insets(0, 25, 0, 0));
        seekerName.setEditable(false);
        seekerName.setFocusable(false);
        seekerName.setPreferredSize(new Dimension(400,19));
        c.fill = GridBagConstraints.HORIZONTAL;
        topPanel.add(seekerName, c);
        addListeners(seekerName);

        c = new GridBagConstraints();

        //Create location field
        JTextArea location = new JTextArea();
        location.setText(user.getCity() == null ? "City Name Here" : user.getCity());
        //location.setBackground(Color.green);
        location.setFont(font);
        location.setEditable(false);
        location.setFocusable(false);
        location.setPreferredSize(new Dimension(300,19));
        c.fill = GridBagConstraints.HORIZONTAL;
        topPanel.add(location, c);
        addListeners(location);

        c = new GridBagConstraints();

        //Create result field
        JTextArea matchResult = new JTextArea();
        //matchResult.setBackground(Color.cyan);
        //matchResult.setText("100%");
        matchResult.setText(result + "%");
        matchResult.setFont(font);
        matchResult.setEditable(false);
        matchResult.setFocusable(false);
        matchResult.setPreferredSize(new Dimension(50,19));

        topPanel.add(matchResult, c);
        addListeners(location);

        //Create open/close button
        openArrow = new ImageIcon("resources/arrow_down.png");
        Image openImg = openArrow.getImage();
        Image openImgScld = openImg.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
        openArrow = new ImageIcon(openImgScld);

        closeArrow = new ImageIcon("resources/arrow_up.png");
        Image closeImg = closeArrow.getImage();
        Image closeImgScld = closeImg.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
        closeArrow = new ImageIcon(closeImgScld);

        arrow = new JLabel();
        //arrow.setPreferredSize(new Dimension(50,30));
        //arrow.setBackground(Color.orange);
        arrow.setIcon(openArrow);
        topPanel.add(arrow, c);
        addListeners(arrow);
    }

    /**
     * Populate the bottom panel with components and user information.
     * @param user
     */
    private void populateBottomPanel(User user, Job job) {
        //CREATE BOTTOM PANEL
        bottomPanel.setLayout(new BoxLayout(bottomPanel,BoxLayout.PAGE_AXIS));

        bottomPanel.add(Box.createRigidArea(new Dimension(-1,5)));

        JButton message = new JButton();
        message.setText("Message");
        message.setBackground(new Color(52,107,187));
        message.setForeground(Color.white);
        message.setFont(font);
        message.setPreferredSize(new Dimension(150,30));

        message.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Clicked message button");
                var messageForm = new MessageForm(FrameManager.getInstance().getMainFrame(),user,job);
            }
        });
        bottomPanel.add(message);

        publishSkills(user);
        publishQualifications(user);
    }

    /**
     * Formats the user qualifications into a grid for display and adds to the bottom panel.
     * @param user
     */
    private void publishQualifications(User user) {
        //Create quals
        JTextArea qualHeading = new JTextArea();
        qualHeading.setLineWrap(true);
        qualHeading.setMargin(new Insets(15, 15, 15, 15));
        qualHeading.setPreferredSize(new Dimension(Short.MAX_VALUE,45));
        qualHeading.setMaximumSize(new Dimension(Short.MAX_VALUE, 45));

        var quals = user.getQualificationTags();
        qualHeading.setText("Qualifications:");
        qualHeading.setFont(font);
        qualHeading.setEditable(false);
        qualHeading.setFocusable(false);

        bottomPanel.add(qualHeading);

        JPanel grid = new JPanel();
        grid.setBackground(bottomPanel.getBackground());
        var gl = new GridLayout(0,5);
        grid.setLayout(gl);
        //grid.setPreferredSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
        if(quals.getTags().size() == 0){
            JTextArea noQuals = new JTextArea();
            noQuals.setText("\u2022 Candidate has no qualifications listed in their profile");
            noQuals.setFont(font);
            grid.add(noQuals);
        }
        else {
            for (Map.Entry<TagKey, Tag> entry : quals.getTags().entrySet()) {
                JTextArea text = new JTextArea();
                text.setLineWrap(true);
                text.setText("\u2022" + entry.getKey().getValue().toLowerCase() + " " + entry.getKey().getName().toLowerCase());
                text.setFont(font);
                text.setEditable(false);
                text.setFocusable(false);
                grid.add(text);

            }
        }
        grid.setBorder(BorderFactory.createEmptyBorder(0,30,15,15));
        bottomPanel.add(grid);
    }

    /**
     * Formats the user skills into a grid for display and adds to the bottom panel.
     * @param user
     */
    private void publishSkills(User user) {
        //Create skills
        JTextArea skillHeading = new JTextArea();
        skillHeading.setLineWrap(true);
        skillHeading.setMargin(new Insets(15, 15, 15, 15));
        skillHeading.setPreferredSize(new Dimension(Short.MAX_VALUE,45));
        skillHeading.setMaximumSize(new Dimension(Short.MAX_VALUE, 45));

        var skills = user.getSkillTags();
        skillHeading.setText("Candidates skills:");
        skillHeading.setFont(font);
        skillHeading.setEditable(false);
        skillHeading.setFocusable(false);

        bottomPanel.add(skillHeading);

        JPanel grid = new JPanel();
        grid.setBackground(bottomPanel.getBackground());
        var gl = new GridLayout(0,5);
        grid.setLayout(gl);
        //grid.setPreferredSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));

        if(skills.getTags().size() == 0){
            JTextArea noSkills = new JTextArea();
            noSkills.setText("\u2022 Candidate has no skills listed in their profile");
            noSkills.setFont(font);
            grid.add(noSkills);
        }
        else {
            for (Map.Entry<TagKey, Tag> entry : skills.getTags().entrySet()) {
                JTextArea text = new JTextArea();
                text.setLineWrap(true);
                text.setText("\u2022" + entry.getKey().getValue().toLowerCase() + " " + entry.getKey().getName().toLowerCase());
                text.setFont(font);
                text.setEditable(false);
                text.setFocusable(false);
                grid.add(text);

            }
        }
        grid.setBorder(BorderFactory.createEmptyBorder(0,30,15,15));
        bottomPanel.add(grid);
    }

    private void AddMouseListener(JComponent component, MouseListener[] mouseListeners) {
        component.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                ToggleExpand();
            }
        });
    }

    @Override
    protected void ToggleExpand() {
        if (!expanded) {
            arrow.setIcon(closeArrow);
        } else {
            arrow.setIcon(openArrow);
        }

        super.ToggleExpand();
    }
}

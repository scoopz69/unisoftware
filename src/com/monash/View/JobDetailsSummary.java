package com.monash.View;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JobDetailsSummary {
    private JTextArea applicants;
    private JTextArea dateCreated;
    private JLabel jobTitle;
    private JButton view;
    private JButton edit;
    private JPanel rootPanel;
    private int jobId;

    public JobDetailsSummary() {
        view.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //TODO: Show applicants screen for jobId
            }
        });
        edit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //TODO: show create/edit job screen for jobId
            }
        });
    }

    public JPanel getRootPanel() {
        return rootPanel;
    }

    public void setRootPanel(JPanel rootPanel) {
        this.rootPanel = rootPanel;
    }

    public JButton getEdit() {
        return edit;
    }

    public void setEdit(JButton edit) {
        this.edit = edit;
    }

    public JButton getView() {
        return view;
    }

    public void setView(JButton view) {
        this.view = view;
    }

    public JLabel getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(JLabel jobTitle) {
        this.jobTitle = jobTitle;
    }

    public JTextArea getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(JTextArea dateCreated) {
        this.dateCreated = dateCreated;
    }

    public JTextArea getApplicants() {
        return applicants;
    }

    public void setApplicants(JTextArea applicants) {
        this.applicants = applicants;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }
}
package com.monash.View;

import javax.lang.model.type.ArrayType;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class AdminConfig {
    private JPanel rootPanel;
    private JTextArea subHeading;
    private JButton save;
    private JTextArea errSave;
    private JButton revertChanges;
    private JScrollPane scrollView;
    private JPanel scrollContent;
    private JTextPane thereAreNoCategoriesTextArea;
    private JButton addCategory;
    private HashMap<Integer, JTextField> categoryFields;
    private ArrayList<Integer> catsToDelete;

    public AdminConfig() {
        scrollView.setBorder(BorderFactory.createEmptyBorder());

        scrollContent.setLayout(new BoxLayout(scrollContent,BoxLayout.PAGE_AXIS));

        ImageIcon icon = new ImageIcon("resources/addition.png");
        Image iconImg = icon.getImage();
        Image imgScld = iconImg.getScaledInstance(30,30,Image.SCALE_SMOOTH);
        ImageIcon iconScld= new ImageIcon(imgScld);
        addCategory.setIcon(iconScld);

        loadCategories();

        addCategory.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                generateSlot(-1,""); //Generate empty input field
                enableRevertChanges(true);

                scrollContent.revalidate();
                scrollContent.repaint();

                //Refresh scroll to bottom of list
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.getVerticalScrollBar().setValue(scrollView.getVerticalScrollBar().getMaximum());
                    }
                });
            }
        });
        revertChanges.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                scrollContent.removeAll();
                loadCategories();

                enableRevertChanges(false);
            }
        });
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                enableRevertChanges(false);

                //Delete categories
                for (Integer numID : catsToDelete) {
                    try {
                        FrameManager.getInstance().getController().getDb().dbDeleteCategory(numID);  //TODO: replace with delete by ID num when available
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                }

                //Update and add categories
                for (Map.Entry<Integer, JTextField> entry : categoryFields.entrySet()) {
                    try {
                        if(entry.getKey() == -1) {
                            //No existing ID, add new category
                            FrameManager.getInstance().getController().getDb().dbAddCategory(entry.getValue().getText());
                        }
                        else{
                            //Existing ID, update existing category
                            FrameManager.getInstance().getController().getDb().dbUpdateCategory(entry.getKey(), entry.getValue().getText());
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
                errSave.setEnabled(true);   //TODO: update message if save unsuccessful
            }
        });
    }

    private void enableRevertChanges(boolean b) {
        errSave.setEnabled(false);
        revertChanges.setEnabled(b);
        revertChanges.setVisible(b);
    }

    /**
     * Gets the categories from the Controller and initiates input slot generation for each category into the Scroll View.
     */
    private void loadCategories() {
        //ArrayList<String> categories = generateFillerData();    //TODO: replace with DB functionality when available
        categoryFields = new HashMap<>();
        catsToDelete = new ArrayList<>();

        HashMap<Integer,String> loadedCategories = FrameManager.getInstance().getController().getCategories();   //TODO: get through Controller, not direct to DB


        if(loadedCategories.size() == 0){
            thereAreNoCategoriesTextArea.setVisible(true);
            return;
        }
        else{
            thereAreNoCategoriesTextArea.setVisible(false);
        }

        for (Map.Entry<Integer, String> entry : loadedCategories.entrySet()) {
            generateSlot(entry.getKey(), entry.getValue());
        }

        scrollView.getVerticalScrollBar().setUnitIncrement(20);

        scrollContent.revalidate();
        scrollContent.repaint();
    }

    /**
     * Generate a category slot from the InputSlot template
     * @param index
     * @param category
     */
    private void generateSlot(int index, String category) {
        var slot = new InputSlot();
        slot.getInput().setText(category);
        categoryFields.put(index, slot.getInput());

        scrollContent.add(slot.getRootPanel());
        var spacer = Box.createRigidArea(new Dimension(-1, 10));
        scrollContent.add(spacer);

        //If deleted, remove the slot from scroll view and add category ID to list for deletion on save
        slot.getDelete().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                catsToDelete.add(index);
                categoryFields.remove(index);
                scrollContent.remove(slot.getRootPanel());
                scrollContent.remove(spacer);

                checkShowMessage();
                enableRevertChanges(true);

                //TODO: are these necessary?
                scrollContent.revalidate();
                scrollContent.repaint();
            }
        });

        //If input field changed, enable revert button
        slot.getInput().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                enableRevertChanges(true);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                enableRevertChanges(true);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                enableRevertChanges(true);
            }
        });


        checkShowMessage();
    }

    private ArrayList<String> generateFillerData() {
        ArrayList<String> data = new ArrayList<>();

        data.add("Accounting");
        data.add("Business");
        data.add("Construction");
        data.add("Engineering");
        data.add("Accounting");
        data.add("Business");
        data.add("Construction");
        data.add("Engineering");
        data.add("Accounting");
        data.add("Business");
        data.add("Construction");
        data.add("Engineering");
        data.add("Accounting");

        return data;
    }

    public JPanel getRootPanel() {
        return rootPanel;
    }

    public void setRootPanel(JPanel rootPanel) {
        this.rootPanel = rootPanel;
    }

    private void checkShowMessage(){
        if(scrollContent.getComponentCount() == 1){
            thereAreNoCategoriesTextArea.setVisible(true);
        }
        else{
            thereAreNoCategoriesTextArea.setVisible(false);
        }
    }
}

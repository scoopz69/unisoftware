package com.monash.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SeekerProfileStaticFrame {

    final static String DETAILS = "Details";
    final static String SKILLS = "Skills";
    final static String APPLIED_JOBS = "Applied Jobs";

    private JPanel rootPanel;
    private JPanel sideBar;
    private JButton contactDetails;
    private JButton appliedJobs;
    private JButton skills;
    private JPanel contentPanel;


    public SeekerProfileStaticFrame() {

        SeekerContactDetails cd = new SeekerContactDetails();
        SeekerSkills sk = new SeekerSkills();
        SeekerAppliedJobs aj = new SeekerAppliedJobs();

        contentPanel.add(cd.getRootPanel(), DETAILS);
        contentPanel.add(sk.getRootPanel(), SKILLS);
        contentPanel.add(aj.getRootPanel(), APPLIED_JOBS);

        CardLayout cl = (CardLayout)(contentPanel.getLayout());

        ToggleButtonColors(contactDetails);

        contactDetails.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ToggleButtonColors(contactDetails);

                cl.show(contentPanel,DETAILS); // use this to show page that us labeled 'SEARCH'

            }
        });


        // get skills page
        skills.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ToggleButtonColors(skills);

                cl.show(contentPanel,SKILLS); // use this to show page that us labeled 'SEARCH'

            }

        });

        // get applied jobs page
        appliedJobs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ToggleButtonColors(appliedJobs);

                cl.show(contentPanel,APPLIED_JOBS); // use this to show page that us labeled 'SEARCH'
            }

        });

    }


    private void ToggleButtonColors(JButton active) {
        //Set all buttons to sidebar light grey (unselected)
        for (Component component : sideBar.getComponents()) {
            if (component instanceof JButton) {
                component.setBackground(new Color(200, 200, 200));
            }
        }

        active.setBackground(new Color(147,149,151));
    }


    public JPanel getRootPanel() {return rootPanel;}


}

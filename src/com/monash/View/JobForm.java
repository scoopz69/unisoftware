package com.monash.View;

import com.monash.model.Job;
import com.monash.model.Tag;
import com.monash.model.TagKey;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class JobForm {
    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public enum Mode { CREATE, EDIT}
    private final String CATEGORY = "CATEGORY";
    private final String SKILL = "SKILL";

    private JTextArea heading;
    private JTextArea subHeading;
    private JPanel content;
    private JButton mainButton;
    private ActionListener actionMain;
    private JPanel rootPanel;
    private JTextField jobTitle;
    private JTextField company;
    private JTextArea errTitle;
    private JTextField location;
    private JFormattedTextField min;
    private JFormattedTextField max;
    private JEditorPane keywords;
    private JComboBox status;
    private JTextArea errCompany;
    private JTextArea errSave;
    private JButton backButton;
    private JEditorPane summary;
    private JEditorPane description;
    private JTextArea errSummary;
    private JTextArea errDescription;
    private JTextArea errSalary;
    private JPanel catScrollContent;
    private JScrollPane catScroll;

    private ArrayList<JCheckBox> checkBoxes;

    private final String OPEN = "Open";
    private final String CLOSED = "Closed";

    /**
     * Default constructor - generates form in 'create' mode
     */
    public JobForm() {
        summary.setBorder(jobTitle.getBorder());
        description.setBorder(jobTitle.getBorder());
        keywords.setBorder(jobTitle.getBorder());
        status.addItem(OPEN);
        status.addItem(CLOSED);
        NumberFormat nf = NumberFormat.getIntegerInstance();
        NumberFormatter nff = new NumberFormatter(nf);
        DefaultFormatterFactory factory = new DefaultFormatterFactory(nff);
        min.setFormatterFactory(factory);
        max.setFormatterFactory(factory);

        catScrollContent.setLayout(new BoxLayout(catScrollContent, BoxLayout.Y_AXIS));
        catScroll.setBorder(BorderFactory.createEmptyBorder());

        loadCategories();

        mainButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(validateFields()) {
                    addJob();
                }
            }
        });
        jobTitle.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                if(jobTitle.getText().isBlank()){
                    errTitle.setEnabled(true);
                    jobTitle.setText(jobTitle.getText().trim());
                }
                else{
                    errTitle.setEnabled(false);
                }

                return true;
            }
        });
        company.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                if(company.getText().isBlank()){
                    errCompany.setEnabled(true);
                    company.setText(company.getText().trim());
                }
                else{
                    errCompany.setEnabled(false);
                }

                return true;
            }
        });
        summary.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                if(summary.getText().length() >= 1 && summary.getText().length() <= 250){
                    errSummary.setEnabled(false);
                }
                else{
                    errSummary.setEnabled(true);
                }
                return true;
            }
        });
        description.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                if(description.getText().length() >= 1 && description.getText().length() <= 1500){
                    errDescription.setEnabled(false);
                }
                else{
                    errDescription.setEnabled(true);
                }
                return true;
            }
        });
        min.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                return verifySalaryNums();
            }
        });
        max.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                return verifySalaryNums();
            }
        });

        errCompany.setEnabled(false);
        errTitle.setEnabled(false);
        errSummary.setEnabled(false);
        errDescription.setEnabled(false);
        errSalary.setEnabled(false);
        errSave.setEnabled(false);


        addEditChangeListeners();

    }

    /**
     * Non-default constructor - generates form in requested mode
     * @param mode
     */
    public JobForm(Mode mode, Job job) throws SQLException {
        this();

        if(mode == Mode.EDIT && job != null){
            jobTitle.setText(job.getTitle());
            company.setText(job.getCompany());
            location.setText(job.getLocation());
            min.setValue(job.getPayLow());
            max.setValue(job.getPayHigh());
            summary.setText(job.getSummary());
            description.setText(job.getDescription());
            status.setSelectedItem((job.isActive()) ? OPEN : CLOSED);

            displaySelectedCategories(job.getJobCategoriesList());
            displayKeywords(job.getJobSkillsList());

            mainButton.setText("Save Changes");
            mainButton.removeActionListener(mainButton.getActionListeners()[0]);
            mainButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(validateFields()){
                        updateJob(job);
                    }
                }
            });
        }

        validateFields();
    }

    /**
     * Adds Listeners to existing form components so that on edit/property changed the 'save success/failed' message is disabled.
     */
    private void addEditChangeListeners() {
        var editListener = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                errSave.setEnabled(false);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                errSave.setEnabled(false);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                errSave.setEnabled(false);
            }
        };

        company.getDocument().addDocumentListener(editListener);
        jobTitle.getDocument().addDocumentListener(editListener);
        summary.getDocument().addDocumentListener(editListener);
        description.getDocument().addDocumentListener(editListener);
        min.getDocument().addDocumentListener(editListener);
        max.getDocument().addDocumentListener(editListener);
        location.getDocument().addDocumentListener(editListener);
        keywords.getDocument().addDocumentListener(editListener);

        status.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                errSave.setEnabled(false);
            }
        });
    }

    /**
     * Initiates adding new job to database through the Controller.
     */
    private void addJob() {

        getSelectedCategories();

        Job newJob = new Job();

        populateJobFields(newJob);

        System.out.println("New job has " + newJob.getJobTagList().size() + " tags and jobID: " + newJob.getJobId());

        boolean success = FrameManager.getInstance().getController().addJob(newJob);

        if(success){
            displaySuccess();
        }
        else{
            displayError();
        }

    }

    /**
     * Sets the keyword string display for the listed strings
     * @param keywordTags
     */
    private void displayKeywords(ArrayList<String> keywordTags){
        System.out.println("Displaying " + keywordTags.size() + " keywords");

        String keyString = "";

        if(keywordTags == null || keywordTags.isEmpty()){
            return;
        }

        for(String tag : keywordTags){
            if(!tag.isBlank()){
                keyString += tag.toLowerCase() + ", ";
            }
        }

        keyString = keyString.trim();

        keywords.setText(keyString);
    }

    /**
     * Sets the category checkbox selections based for the listed Tags
     * @param categoryTags
     */
    private void displaySelectedCategories(ArrayList<Tag> categoryTags) {
        for (Tag tag : categoryTags) {
            System.out.println("Attempting category match for [tagID: " + tag.getId() + " tagKeyType: " + tag.getKey().getType() + " tagKeyName: " + tag.getKey().getName() + " tagKeyValue: " + tag.getKey().getValue());

            for (JCheckBox box : checkBoxes) {
                if(box.getText().equalsIgnoreCase(tag.getKey().getName())){
                    box.setSelected(true);
                }
                else{
                    System.out.println(tag.getKey().getName() + " doesn't match " + box.getText());
                }
            }
        }

        catScrollContent.revalidate();
        catScrollContent.repaint();
    }

    /**
     * Gets the root JPanel used to display the form.
     * @return
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }

    private ArrayList<Tag> getKeywords(){
        ArrayList<Tag> keywordTags = new ArrayList<>();

        String[] keyString = keywords.getText().split(",");

        for(int i = 0; i < keyString.length; i++){
            keyString[i].replaceAll(",","");

            if(!keyString[i].isBlank()){
                Tag tag = new Tag();
                TagKey key = new TagKey();
                key.setType("SKILL");
                key.setName(keyString[i].trim());
                tag.setKey(key);

                keywordTags.add(tag);
            }
        }
        System.out.println("Job has " + keywordTags.size() + " keywords in list");

        return keywordTags;
    }

    /**
     * Get the selected categories that are to be applied to the Job as a Tag
     * @return  array of categories as Tags.
     */
    private ArrayList<Tag> getSelectedCategories(){
        ArrayList<Tag> catTags = new ArrayList<>();

        for( JCheckBox box : checkBoxes) {
            if(box.isSelected()){
                Tag tag = new Tag();
                TagKey key = new TagKey();
                key.setType(CATEGORY);
                key.setName(box.getText());

                tag.setKey(key);

                catTags.add(tag);
            }
        }
        System.out.println("Job has " + catTags.size() + " selected categories");

        return catTags;
    }

    /**
     * Loads categories from the admins configuration into selectable list for recruiter to assign categories.
     */
    private void loadCategories() {
        checkBoxes = new ArrayList<>();
        catScrollContent.removeAll();

        HashMap<Integer,String> categories = FrameManager.getInstance().getController().getCategories();

        for (Map.Entry<Integer,String> entry : categories.entrySet()) {
            JCheckBox check = new JCheckBox(entry.getValue());
            check.setActionCommand(entry.getValue());
            check.setFont(new Font("Arial", Font.PLAIN, 16));
            check.setBackground(Color.white);
            check.setForeground(Color.black);
            check.setMinimumSize(new Dimension(500,30));

            //Edit listener
            check.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    errSave.setEnabled(false);
                }
            });

            checkBoxes.add(check);
            System.out.println("Adding to checkboxes hashmap: " + entry.getValue());
            catScrollContent.add(check);
            catScrollContent.add(Box.createRigidArea(new Dimension(-1, 10)));
        }

        catScroll.getVerticalScrollBar().setUnitIncrement(20);
        catScrollContent.revalidate();
        catScrollContent.repaint();
    }

    /**
     * Parses the text from the formatted number field to an Integer.
     * @param min
     * @return
     */
    private int parseSalary(JFormattedTextField min) {
        //TODO: maybe get rid of this? Its only one line, see if can add more common code here
        return Integer.parseInt(min.getText().replaceAll(",", ""));
    }

    private void populateJobFields(Job newJob) {
        int minSalary = parseSalary(min);
        int maxSalary = parseSalary(max);

        newJob.setTitle(jobTitle.getText());
        newJob.setCompany(company.getText());
        newJob.setPayLow(minSalary);
        newJob.setPayHigh(maxSalary);
        newJob.setLocation(location.getText());
        newJob.setActive(status.getSelectedItem().equals(OPEN));
        newJob.setRecruiterId(FrameManager.getInstance().getController().getSessionUserId());
        newJob.setSummary(summary.getText());
        newJob.setDescription(description.getText());

        ArrayList<Tag> allTags = new ArrayList<>();
        allTags.addAll(getKeywords());
        allTags.addAll(getSelectedCategories());

        newJob.setJobTagList(allTags);

        System.out.println("New job has " + newJob.getJobTagList().size() + " tags");

    }

    /**
     * Initiates panel swap to return to Recruiter Job Management screen on either the Open or Closed pages.
     * @param showOpen
     */
    private void returnToJobsManagement(boolean showOpen){
        if(showOpen){
            var openJobs = new RecruiterJobSummary(true);
            FrameManager.getInstance().swapIntoPanel((JPanel) rootPanel.getParent(), openJobs.getRootPanel());
        }
        else{
            var closed = new RecruiterJobSummary(false);
            FrameManager.getInstance().swapIntoPanel((JPanel) rootPanel.getParent(), closed.getRootPanel());
        }
    }

    /**
     * Sets which screen the back button should return to when pressed.
     * @param returnToOpen
     */
    public void setBackButtonToOpenJobs(boolean returnToOpen){
        if(returnToOpen){
            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    var openJobs = new RecruiterJobSummary(true);
                    FrameManager.getInstance().swapIntoPanel((JPanel) rootPanel.getParent(), openJobs.getRootPanel());
                }
            });
        }
        else{
            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    var closedJobs = new RecruiterJobSummary(false);
                    FrameManager.getInstance().swapIntoPanel((JPanel) rootPanel.getParent(), closedJobs.getRootPanel());
                }
            });
        }
    }

    /**
     * Update the job data with the latest information in form fields.
     * @param job
     */
    private void updateJob(Job job) {
        if (validateFields()) {
            Job updatedJob = new Job();
            updatedJob.setJobId(job.getJobId());
            populateJobFields(updatedJob);

            for(int i = 0; i < updatedJob.getJobTagList().size(); i++) {
                System.out.println("Tag check added to job tagList: " + updatedJob.getJobTagList().get(i).getKey().getName());
            }
            boolean success = FrameManager.getInstance().getController().updateJob(updatedJob);

            if(success){
                displaySuccess();
            }
            else{
                displayError();
            }
        }

        //returnToJobsManagement(status.getSelectedItem().equals(OPEN));
    }

    private void displayError() {
        errSave.setForeground(Color.RED);
        errSave.setText("There was an error while saving");
        errSave.setEnabled(true);
    }

    private void displaySuccess() {
        errSave.setForeground(new Color(52,107,187));
        errSave.setText("Saved successfully");
        errSave.setEnabled(true);
    }

    /**
     * Triggers verification of fields and checks that no errors are found and displayed.
     * @return
     */
    private boolean validateFields() {
        jobTitle.getInputVerifier().verify(jobTitle);
        company.getInputVerifier().verify(company);
        min.getInputVerifier().verify(min);
        max.getInputVerifier().verify(max);
        summary.getInputVerifier().verify(summary);
        description.getInputVerifier().verify(description);

        if(errSalary.isEnabled() || errSummary.isEnabled() || errDescription.isEnabled() || errTitle.isEnabled() || errCompany.isEnabled()){
            return false;
        }

        return true;
    }

    /**
     * Checks the minimum and maximum salary inputs and determines if there is an error.
     * @return
     */
    private boolean verifySalaryNums() {
        if(min.getText().isBlank() || max.getText().isBlank()){
            errSalary.setEnabled(true);
        }
        try{
            int minSalary = parseSalary(min);
            int maxSalary = parseSalary(max);

            if(minSalary < 0){
                min.setText("0");
            }
            if(minSalary > maxSalary){
                errSalary.setEnabled(true);
            }
            else{
                errSalary.setEnabled(false);
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        return true;
    }

}

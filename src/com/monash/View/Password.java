package com.monash.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Password {
    private JPanel rootPanel;
    private JPanel centrePanel;
    private JPanel passwordPanel;
    private JButton saveButton;
    private JLabel currentPassLabel;
    private JTextField currentPassText;
    private JLabel newPassLabel;
    private JLabel repeatLabel;
    private JPasswordField passwordField1;
    private JPasswordField passwordField2;
    private JPasswordField passwordField3;
    private JTextArea errorMsg;
    private JPanel errMsg;
    private JTextArea resultsDisplayedHereTextArea;

    public Password() {
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String newPass = String.valueOf(passwordField2.getPassword());
                String repeatPass = String.valueOf(passwordField3.getPassword());

                try {
                   String returnString = updatePassword(newPass,repeatPass);
                    errorMsg.setForeground(Color.RED);
                    errorMsg.setText(returnString);
                    errorMsg.setEnabled(true);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public JPanel getRootPanel() {
        return rootPanel;
    }
    private String updatePassword(String newPass, String repeatPass) throws SQLException {

        int userId = FrameManager.getInstance().getController().getSessionUserId();
        String compare1 = newPass;
        String compare2 = repeatPass;
        if (compare1.equals(compare2)) {
            try {
                return FrameManager.getInstance().getController().updatePassword(userId, newPass);
            } catch (Exception error) {
                System.out.println(error.getMessage());
            }
        }
        return "Error, try again";
    }
}

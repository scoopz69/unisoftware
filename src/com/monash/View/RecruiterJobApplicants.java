package com.monash.View;

import com.monash.model.Job;
import com.monash.model.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * GUI Display to show applicants for a selected job.
 * @author Lea Walton
 */
public class RecruiterJobApplicants {
    private JPanel rootPanel;
    private JTextArea heading;
    private JTextArea subHeading;
    private JPanel content;
    private JPanel headerRow;
    private JScrollPane scrollView;
    private JPanel scrollContent;
    private JPanel selectedJob;
    private JTextArea selectedNumApp;
    private JTextArea selectedJobTitle;
    private JTextArea selectedJobDate;
    private JButton backButton;
    private ArrayList<User> applicants;

    public RecruiterJobApplicants(){
        scrollView.setBorder(BorderFactory.createEmptyBorder());

        scrollContent.setLayout(new BoxLayout(scrollContent,BoxLayout.PAGE_AXIS));
    }

    public RecruiterJobApplicants(Job job, ArrayList<User> applicants){
        this();

        this.applicants = applicants;

        selectedJobTitle.setText(job.getTitle());
        try {
            var numApp = FrameManager.getInstance().getController().getJobApplicants(job.getJobId());
            selectedNumApp.setText(String.valueOf(numApp.size()));
        } catch (SQLException e) {
            e.printStackTrace();
            selectedNumApp.setText("-1");
        } //TODO replace
        selectedJobDate.setText((job.getDate() != null) ? job.getDate().toString() : "DD MMM YYYY");

        loadApplicants(job);
    }

    private void loadApplicants(Job job) {
//            System.out.println("Job: " + job.toString());
        for (User user: applicants) {
//            System.out.println("User ID: " + user.getId());
//            System.out.println("Num of tags: " + user.getAllTags().count());
//            System.out.println("Match: " + FrameManager.getInstance().getController().doTalentMatch(user, job));

            var panel = new SeekerPanel(user,FrameManager.getInstance().getController().doTalentMatch(user,job).getResult(), job);
            scrollContent.add(panel.getRootPanel());
            scrollContent.add(Box.createRigidArea(new Dimension(-1, 10)));
        }

        //prevent all child height resized
//        scrollContent.add(new Box.Filler(new Dimension(0, 0),
//         new Dimension(0, Short.MAX_VALUE), new Dimension(0, Short.MAX_VALUE)));

        scrollContent.add(Box.createGlue());

        scrollView.getVerticalScrollBar().setUnitIncrement(20);

        scrollView.revalidate();
        scrollView.repaint();

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                scrollView.getVerticalScrollBar().setValue(0);
            }
        });
    }

    public JTextArea getSelectedJobDate() {
        return selectedJobDate;
    }

    public void setSelectedJobDate(JTextArea selectedJobDate) {
        this.selectedJobDate = selectedJobDate;
    }


    public JButton getBackButton() {
        return backButton;
    }

    public void setBackButton(JButton backButton) {
        this.backButton = backButton;
    }

    public JPanel getRootPanel() {
        return rootPanel;
    }

    public void setRootPanel(JPanel rootPanel) {
        this.rootPanel = rootPanel;
    }

    public void setBackButtonToOpenJobs(boolean returnToOpen){
        if(returnToOpen){
            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    var openJobs = new RecruiterJobSummary(true);
                    FrameManager.getInstance().swapIntoPanel((JPanel) rootPanel.getParent(), openJobs.getRootPanel());
                }
            });
        }
        else{
            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    var openJobs = new RecruiterJobSummary(false);
                    FrameManager.getInstance().swapIntoPanel((JPanel) rootPanel.getParent(), openJobs.getRootPanel());
                }
            });
        }
    }

    public ArrayList<User> getApplicants() {
        return applicants;
    }

    public void setApplicants(ArrayList<User> applicants) {
        this.applicants = applicants;
        //loadApplicants();
    }
}

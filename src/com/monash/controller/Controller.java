package com.monash.controller;

import com.monash.View.FrameManager;
import com.monash.model.*;
import com.monash.model.Search.JobSearch;
import com.monash.model.Search.SearchResult;
import com.monash.model.Search.SeekerSearch;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;


public class Controller {
    private Database db;
    private FrameManager frameManager;
    private InputVerification inputVerification;
    private User sessionUser;

    public Controller(Database db, FrameManager frameManager) {
        this.db = db;
        this.frameManager = frameManager;
        this.inputVerification = new InputVerification();
    }

    public void loadRecData(int recId) {
        try {
            db.loadRecruiterData(recId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void loadSeekData() throws SQLException {
        try {
            db.loadSeekerData();
        }
        catch(SQLException e) {
            e.printStackTrace();        }
    }

    public ArrayList<User> getUserList() {
        return db.getUserList();
    }

    public ArrayList<Job> getRecruiterJobList() {
        try {
            return db.getRecruiterJobList();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<Job> jobList = new ArrayList<>();
        return jobList;
    }

    public ArrayList<Message> getMessageList(User user) throws SQLException {
        int userId = user.getId();
        boolean isRecruiter = user.getRecruiter();

        if (isRecruiter) {
            return db.dbGetRecruiterMessages(userId);
        } else {
            return db.dbGetSeekerMessages(userId);
        }
    }

    public void updateSeekerSkill(int userId, ArrayList<Tag> tagList) throws SQLException {
        db.dbUpdateSeekerSkill(userId,tagList);
    }

    public ArrayList<Job> getSearchResults(String searchString) {
        return new ArrayList<Job>();
    }

    public HashMap<Integer, String> getCategories() {
        try {
            return db.dbGetCategories();
        } catch (SQLException e) {
            return new HashMap<>();
        }
    }

    public String registerUser(String password, String firstName, String lastName, String email, Boolean isRecruiter) throws SQLException {
        return db.databaseAddUser(password, firstName, lastName, email, isRecruiter);
    }

    public String updateUserProfile(int userId, String firstName, String lastName, String email) throws SQLException {
        return db.dbUpdateUserProfile(userId, firstName, lastName, email);
    }

    public String updatePassword(int userId, String password) throws SQLException {
        return db.dbUpdatePassword(userId, password);
    }

    //TODO change to message object constructor
    public boolean addMessage(String sender, String roleName, String messageContent, boolean read, int sender_id, int receiver_id) {
        try {
            db.dbAddMessage(sender, roleName, messageContent, read, sender_id, receiver_id);
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean addJob(Job job) {
        try {
            db.dbAddJob(job);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateJob(Job job) {
        try {
            db.dbUpdateJob(job);
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    //TODO change to application object
    public void addApplication(int jobId, int recruiterId, int seekerId) throws SQLException {
        db.dbAddApp(jobId, recruiterId, seekerId);
    }

    public ArrayList<User> getJobApplicants(int jobId) throws SQLException {
        return db.dbGetJobApplicants(jobId);
    }

    public Database getDb() {
        return db;
    }

    public void setDb(Database db) {
        this.db = db;
    }

    public User getUser() {
        return this.sessionUser;
    }

    public int getSessionUserId() {
        return this.sessionUser.getId();
    }

    public void setUser(User user) {
        this.sessionUser = user;
    }

    public InputVerification getInputVerification() {
        return inputVerification;
    }

    public void setInputVerification(InputVerification inputVerification) {
        this.inputVerification = inputVerification;
    }

    /**
     * Execute a job search
     *
     * @return Search results
     */
    public ArrayList<SearchResult<Job>> doJobSearch(ArrayList<String> categories,
                                                    String searchTerms,
                                                    String separator,
                                                    int payFrom,
                                                    int payTo) {
        SeekerSearch seekerSearch = new SeekerSearch();
        seekerSearch.setRequester(sessionUser);
        seekerSearch.setProvider(db.getJobsList());
        seekerSearch.go(categories, searchTerms, separator, payFrom, payTo);
        seekerSearch.sort();

        return seekerSearch.getResults();
    }

    /**
     * Match talent to a job
     *
     * @param job Job to be matched with
     * @return ArrayList of SearchResult<User> objects, sorted in descending order of match percentage
     */
    public ArrayList<SearchResult<User>> doTalentMatch(Job job) {
        JobSearch jobSearch = new JobSearch();
        jobSearch.setRequester(job);
        jobSearch.setProvider(db.getUserList());
        jobSearch.go();
        jobSearch.sort();

        return jobSearch.getResults();
    }

    /**
     * Match a single user to a single job
     *
     * @param user User
     * @param job  Job
     * @return Match result
     */
    public SearchResult<User> doTalentMatch(User user, Job job) {
        SearchResult<User> result = new SearchResult<User>();

        ArrayList<User> users = new ArrayList<User>();
        users.add(user);

        JobSearch jobSearch = new JobSearch();
        jobSearch.setRequester(job);
        jobSearch.setProvider(users);
        jobSearch.go();

        if (!jobSearch.getResults().isEmpty()) {
            result = jobSearch.getResults().get(0);
        }
        else {
            result.setEntity(user);
            result.setResult(0);
        }

        return result;
    }

    //on signout clears locally stored session data
    public void clearData(){
        db.clearData();
    }
}
package com.monash.controller;

import com.monash.View.FrameManager;

public class InputVerification {

    public boolean nameIsValid(String text) {
        if (text.isBlank()) {
            return false;
        }
        return true;
    }

    public boolean emailIsValid(String email) {
        //Source: https://stackoverflow.com/questions/624581/what-is-the-best-java-email-address-validation-method
        //TODO: verify regex
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public boolean passwordIsValid(String password) {
        if (password.length() < 6) {
            return false;
        }

        return true;
    }

    public boolean checkPasswordsMatch(String password1, String password2) {
        if (password1.equals(password2)) {
            return true;
        }

        return false;
    }

    public boolean usernameAvailable(String email) {
        var users = FrameManager.getInstance().getController().getDb().getUserList();

        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getEmail().equals(email)) {
                return false;
            }
        }

        return true;
    }

    public boolean verifyLength(String text, int min, int max) {
        if (text.length() >= min && text.length() <= max) {
            return true;
        }
        return false;
    }
}

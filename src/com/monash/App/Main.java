package com.monash.App;

import com.monash.View.FrameManager;
import com.monash.View.MainLogin;
import com.monash.controller.Controller;
import com.monash.model.*;


import javax.swing.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel( UIManager.getCrossPlatformLookAndFeelClassName() );
        } catch (Exception e) {
            e.printStackTrace();
        }


        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                //TODO - insert login + registration panel checks, set mainframe to not visible until verified
                try {
                    runApp();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public static void runApp() throws Exception {
        Database db = new Database();
        FrameManager manager = new FrameManager();
        FrameManager.getInstance().getMainFrame().setSize(1920,1080);
        MainLogin login = new MainLogin();

        FrameManager.getInstance().getMainFrame().setContentPane(login.getRootPanel());
        manager.getMainFrame().setVisible(true);
        Controller controller = new Controller(db, manager);
        FrameManager.getInstance().setController(controller);
        db.loadStartData();

        //TODO keep for testing tags and jobs remove at end
        //Test skill tags //
        /*Job jobTest = new Job(16,"we need java developers", "java developer", "software r us", "Melbourne", "need a java dev",40000,80000,0,true,95,null,0);
        ArrayList<Tag> skillTagList = new ArrayList<>();
        TagKey tagKey = new TagKey("SKILL","java","");
        Tag tag = new Tag();
        tag.setKey(tagKey);
        skillTagList.add(tag);
        jobTest.setJobTagList(skillTagList);
       */


    }

}

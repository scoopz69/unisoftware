package com.monash.model;

import com.monash.model.Search.TagList;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Job implements Comparable<Job> {

    private int jobId;
    private String summary;
    private Date date;
    private String title;
    private String company;
    private String location;
    private String description;
    private int payLow;
    private int payHigh;
    private int tagListId;
    private boolean active;
    private int recruiterId;
    private ArrayList<Tag> jobTagList;
    private ArrayList<String> jobSkillsList;
    private ArrayList<Tag> jobCategoriesList;
    private int searchMatch;
    //TODO rethink placement
    private ArrayList<User> matchedUserList;

    public Job() {
        jobSkillsList = new ArrayList<>();
        jobCategoriesList = new ArrayList<>();
    }

    // no date;
    public Job(int jobId, String summary, String title, String company, String location, String description, int payLow,
               int payHigh, int tagListId, boolean active, int recruiterId, ArrayList<User> matchedUserList, int searchMatch) {
        this.jobId = jobId;
        this.summary = summary;
        this.title = title;
        this.company = company;
        this.location = location;
        this.description = description;
        this.payLow = payLow;
        this.payHigh = payHigh;
        this.tagListId = tagListId;
        this.active = active;
        this.recruiterId = recruiterId;
        this.matchedUserList = matchedUserList;
        this.jobTagList = new ArrayList<>();
        this.searchMatch = searchMatch;


        jobSkillsList = new ArrayList<>();
        jobCategoriesList = new ArrayList<>();
    }

    public Job( String summary, String title, String company, String location, String description, int payLow,
                int payHigh, int tagListId, boolean active, int recruiterId, ArrayList<User> matchedUserList, int searchMatch) {

        this.summary = summary;
        this.title = title;
        this.company = company;
        this.location = location;
        this.description = description;
        this.payLow = payLow;
        this.payHigh = payHigh;
        this.tagListId = tagListId;
        this.active = active;
        this.recruiterId = recruiterId;
        this.matchedUserList = matchedUserList;
        this.jobTagList = new ArrayList<>();
        this.searchMatch = searchMatch;


        jobSkillsList = new ArrayList<>();
        jobCategoriesList = new ArrayList<>();
    }

    public Job(int jobId, String summary, Date date, String title, String company, String location, String description,
               int payLow, int payHigh, int tagListId, boolean active, int recruiterId) {
        this.jobId = jobId;
        this.summary = summary;
        this.date = date;
        this.title = title;
        this.company = company;
        this.location = location;
        this.description = description;
        this.payLow = payLow;
        this.payHigh = payHigh;
        this.tagListId = tagListId;
        this.active = active;
        this.recruiterId = recruiterId;
        this.jobTagList = new ArrayList<>();


        jobSkillsList = new ArrayList<>();
        jobCategoriesList = new ArrayList<>();
    }

    public void addJobListTag(Tag tag) {
        this.jobTagList.add(tag);
    }

    public ArrayList<String> getJobSkillsList() {
        jobSkillsList.clear();

        for(Tag tag: jobTagList) {
            if(tag.getKey().getType().equals("SKILL")) {
                jobSkillsList.add(tag.getKey().getName());
                System.out.println(title + " has tag " + tag);
            }
        }
        return jobSkillsList;
    }

    public void setJobSkillsList(ArrayList<String> jobSkillsList) {
        this.jobSkillsList = jobSkillsList;
    }

    public ArrayList<Tag> getJobCategoriesList() {
        jobCategoriesList.clear();

        for(Tag tag: jobTagList) {
            if(tag.getKey().getType().equals("CATEGORY")) {
                jobCategoriesList.add(tag);
                System.out.println(title + " has tag " + tag);
            }
        }
        return jobCategoriesList;
    }

    public void setJobCategoriesList(ArrayList<Tag> jobCategoriesList) {
        this.jobCategoriesList = jobCategoriesList;
    }

    public ArrayList<Tag> getJobTagList() {
        return jobTagList;
    }

    /**
     * Get the job tags as a TagList
     *
     * @return Job TagList
     */
    public TagList<Job> getJobTags() {
        TagList<Job> tagList = new TagList<Job>();
        HashMap<TagKey, Tag> tagMap = new HashMap<TagKey, Tag>();

        for (Tag tag : jobTagList)
            tagMap.putIfAbsent(tag.getKey(), tag);

        tagList.setOwner(this);
        tagList.setTags(tagMap);

        return tagList;
    }

    /**
     * Get job tags of a given type as a TagList
     *
     * @param type Job type
     * @return TagList
     */
    public TagList<Job> getJobTags(String type) {
        TagList<Job> tagList = new TagList<Job>();
        HashMap<TagKey, Tag> tagMap = new HashMap<TagKey, Tag>();

        for (Tag tag : jobTagList)
            if (tag.getKey().getType().equalsIgnoreCase(type))
                tagMap.putIfAbsent(tag.getKey(), tag);

        tagList.setOwner(this);
        tagList.setTags(tagMap);

        return tagList;
    }

    /**
     * Get job tags of a given type and name as a TagList
     *
     * @param type Tag type
     * @param name Tag name
     * @return Job TagList
     */
    public TagList<Job> getJobTags(String type, String name) {
        TagList<Job> tagList = new TagList<Job>();
        HashMap<TagKey, Tag> tagMap = new HashMap<TagKey, Tag>();

        for (Tag tag : jobTagList)
            if (tag.getKey().getType().equalsIgnoreCase(type) && tag.getKey().getName().equalsIgnoreCase(name))
                tagMap.putIfAbsent(tag.getKey(), tag);

        tagList.setOwner(this);
        tagList.setTags(tagMap);

        return tagList;
    }

    public void setJobTagList(ArrayList<Tag> jobTagList) {
        this.jobTagList = jobTagList;
    }

    public void setMatchedUserList(ArrayList<User> matchedUserList) {
        this.matchedUserList = matchedUserList;
    }

    public int getSearchMatch() {
        return searchMatch;
    }

    public void setSearchMatch(int searchMatch) {
        this.searchMatch = searchMatch;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPayLow() {
        return payLow;
    }

    public void setPayLow(int payLow) {
        this.payLow = payLow;
    }

    public int getPayHigh() {
        return payHigh;
    }

    public void setPayHigh(int payHigh) {
        this.payHigh = payHigh;
    }

    public int getTagListId() {
        return tagListId;
    }

    public void setTagListId(int tagListId) {
        this.tagListId = tagListId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getRecruiterId() {
        return recruiterId;
    }

    public void setRecruiterId(int recruiterId) {
        this.recruiterId = recruiterId;
    }

    public ArrayList<User> getMatchedUserList() {
        return matchedUserList;
    }

    public void addMatchedUserList(User user) {
        this.matchedUserList.add(user);
    }

    public ArrayList<String> getJobKeyWords() {

        //TODO get the users skills as arrayList
        ArrayList<String> blankArray = new ArrayList<>();

        return blankArray;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String searchText() {
        String result = summary + " " +
                        title +  " " +
                        company + " " +
                        location + " " +
                        description + " ";

        TagList<Job> tags = getJobTags("SKILL");
        HashMap<TagKey, Tag> jobTags = tags.getTags();

        for (Map.Entry<TagKey, Tag> tag : jobTags.entrySet())
            result += tag.getKey().getType() + " " +
                      tag.getKey().getName() + " " +
                      tag.getKey().getValue() + " ";

        return result;
    }

    @Override
    public String toString() {
        return "Job{" +
                "jobId=" + jobId +
                ", summary='" + summary + '\'' +
                ", date=" + date +
                ", title='" + title + '\'' +
                ", company='" + company + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", payLow=" + payLow +
                ", payHigh=" + payHigh +
                ", tagListId=" + tagListId +
                ", active=" + active +
                ", recruiterId=" + recruiterId +
                ", matchedUserList=" + matchedUserList +
                ", jobSkillList=" + jobTagList +
                ", searchMatch=" + searchMatch +
                '}';
    }

    @Override
    public int compareTo(Job o) {
        return this.getJobId() - o.getJobId();
    }
}
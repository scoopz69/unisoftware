package com.monash.model;

import java.util.Date;

public class Message {
    private int messageId;
    private String sender;
    private Date date;
    private String roleName;
    private String messageContent;
    private Boolean read;
    private int senderId;
    private int receiverId;

    public Message(int messageId, String sender, Date date, String roleName, String messageContent, Boolean read, int senderId, int receiverId) {
        this.messageId = messageId;
        this.sender = sender;
        this.date = date;
        this.roleName = roleName;
        this.messageContent = messageContent;
        this.read = read;
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

    //TODO double check no usage and remove
    public Message(String sender, Date date, String roleName, String messageContent, Boolean read, int senderId, int receiverId) {
        this.sender = sender;
        this.date = date;
        this.roleName = roleName;
        this.messageContent = messageContent;
        this.read = read;
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public int getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getDate() {
        String tempDate = this.date.toString();
        return tempDate;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }
}
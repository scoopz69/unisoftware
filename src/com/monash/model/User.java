package com.monash.model;

import com.monash.model.Search.TagList;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class User implements Comparable<User> {

    private int id;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private Boolean isRecruiter;
    private int tagListId;
    private int matchPercent;
    private String city;
    private ArrayList<Tag> skillTagList;
    private ArrayList<Tag> qualifTagList;


    public User() {

    }

    public User(int id, String password, String firstName, String lastName, String email, Boolean isRecruiter, int tagListId, int matchPercent, String city, ArrayList<Tag> skillTagList, ArrayList<Tag> qualifTagList) {
        this.id = id;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.isRecruiter = isRecruiter;
        this.tagListId = tagListId;
        this.matchPercent = matchPercent;
        this.city = city;
        this.skillTagList = skillTagList;
        this.qualifTagList = qualifTagList;
    }


    // with password
    public User(String password, String firstName, String lastName, String email, Boolean isRecruiter, int tagListId, int matchPercent, String city, ArrayList<Tag> skillTagList, ArrayList<Tag> qualifTagList) {
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.isRecruiter = isRecruiter;
        this.tagListId = tagListId;
        this.matchPercent = matchPercent;
        this.city = city;
        this.skillTagList = skillTagList;
        this.qualifTagList = qualifTagList;
    }

    //constructor with no Password, no id
    public User(String firstName, String lastName, String email, Boolean isRecruiter, int tagListId, int matchPercent, String city, ArrayList<Tag> skillTagList, ArrayList<Tag> qualifTagList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.isRecruiter = isRecruiter;
        this.tagListId = tagListId;
        this.matchPercent = matchPercent;
        this.city = city;
        this.skillTagList = skillTagList;
        this.qualifTagList = qualifTagList;
    }

    public User(int id, String password, String firstName, String lastName, String email, Boolean isRecruiter, int matchPercent, String city) {
        this.id = id;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.isRecruiter = isRecruiter;
        this.matchPercent = matchPercent;
        this.city = city;
        this.skillTagList = new ArrayList<Tag>();
        this.qualifTagList = new ArrayList<Tag>();
    }

    public ArrayList<String> getUserKeyWords(){
        ArrayList<String> blankArray = new ArrayList<>();
        return blankArray;
    }

    public ArrayList<Tag> mergeTags() {
        ArrayList<Tag> merge = new ArrayList<Tag>();

        for (Tag tag : skillTagList)
            merge.add(tag);

        for (Tag tag : qualifTagList)
            merge.add(tag);

        return merge;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getRecruiter() {
        return isRecruiter;
    }

    public void setRecruiter(Boolean recruiter) {
        isRecruiter = recruiter;
    }

    public int getTagListId() {
        return tagListId;
    }

    public void setTagListId(int tagListId) {
        this.tagListId = tagListId;
    }

    public int getMatchPercent() {
        return matchPercent;
    }

    public void setMatchPercent(int matchPercent) {
        this.matchPercent = matchPercent;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public ArrayList<Tag> getSkillTagList() {
        return skillTagList;
    }

    /**
     * Get the skill tags as a tag list
     *
     * @return Skills TagList
     */
    public TagList<User> getSkillTags() {
        TagList<User> tagList = new TagList<User>();
        HashMap<TagKey, Tag> tagMap = new HashMap<TagKey, Tag>();

        for (Tag tag : skillTagList)
            tagMap.putIfAbsent(tag.getKey(), tag);

        tagList.setOwner(this);
        tagList.setTags(tagMap);

        return tagList;
    }

    /**
     * Get the qualification tags as a tag list
     *
     * @return Qualifications TagList
     */
    public TagList<User> getQualificationTags() {
        TagList<User> tagList = new TagList<User>();
        HashMap<TagKey, Tag> tagMap = new HashMap<TagKey, Tag>();

        for (Tag tag : qualifTagList)
            tagMap.putIfAbsent(tag.getKey(), tag);

        tagList.setOwner(this);
        tagList.setTags(tagMap);

        return tagList;
    }

    /**
     * Get all the user tags in a single TagList
     *
     * @return TagList of all user tags
     */
    public TagList<User> getAllTags() {
        TagList<User> tagList = new TagList<User>();
        TagList<User> skillTags = getSkillTags();
        TagList<User> qualfTags = getQualificationTags();

        for (Tag tag : skillTags.toArrayList())
            tagList.add(tag);

        for (Tag tag : qualfTags.toArrayList())
            tagList.add(tag);

        return tagList;
    }

    public void addSkillTag(Tag tag) {
        this.skillTagList.add(tag);
        //System.out.println("skill tage added user: " + this.getId());
    }

    public void addQualiTag(Tag tag) {
        this.qualifTagList.add(tag);
    }
    public void setSkillTagList(ArrayList<Tag> skillTagList) {
        this.skillTagList = skillTagList;
    }

    public ArrayList<Tag> getQualifTagList() {
        return qualifTagList;
    }

    public void setQualifTagList(ArrayList<Tag> qualifTagList) {
        this.qualifTagList = qualifTagList;
    }

    public boolean isRecruiter() {
        return false;
    }

    @Override
    public int compareTo(User o) {
        return this.getId() - o.getId();
    }
}
package com.monash.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Properties;

/**
 * JSS System Utilities
 *
 * @author ITO5136-TP6-21 Team D
 * @version 0.1
 */
public class Util {
    /**
     * Convert a date to LocalDateTime
     * @param date Date to convert
     * @return LocalDateTime
     */
    public static LocalDateTime localDT(Date date) {
        return Instant
                .ofEpochMilli(date.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    /**
     * Get the application properties
     * @return Application properties
     */
    public static Properties getProperties() {
        FileInputStream propFile;
        Properties prop;

        try {
            propFile = new FileInputStream("system.properties");
            prop = new Properties(System.getProperties());
            prop.load(propFile);
        } catch (Exception e) {
            return null;
        }

        return prop;
    }

    /**
     * Remove all punctuation from a string
     * @param text Source text
     * @return Source text without punctuation
     */
    public static String stripPunctuation(String text) {
        return text.replaceAll("[^A-Za-z0-9 ]+", "");
    }
}

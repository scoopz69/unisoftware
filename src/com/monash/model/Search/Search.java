package com.monash.model.Search;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Master search object
 *
 * @author   ITO5136-TP6-2021 Team D
 * @version  0.0.1
 */
public abstract class Search<R, P> {
    protected ArrayList<SearchResult<P>> results;
    protected ArrayList<P> provider;
    protected TagSearch<P> tagSearch;
    protected TextSearch textSearch;
    protected R requester;

    /**
     * Default constructor
     */
    public Search() {
        results = new ArrayList<SearchResult<P>>();
        tagSearch = new TagSearch<P>();
        provider = new ArrayList<P>();
        textSearch = new TextSearch();
        requester = null;
    }

    /**
     * Constructor
     *
     * @param requester Requesting object
     * @param provider  Search data provider objects
     */
    public Search(R requester, ArrayList<P> provider) {
        this();
        this.requester = requester;
        this.provider = provider;
    }

    /**
     * Get the search results
     *
     * @return Search results
     */
    public ArrayList<SearchResult<P>> getResults() {
        return results;
    }

    /**
     * Set the search results
     *
     * @param results Search results
     */
    public void setResults(ArrayList<SearchResult<P>> results) {
        this.results = results;
    }

    /**
     * Get the tag search object
     *
     * @return Tag search object
     */
    public TagSearch<P> getTagSearch() {
        return tagSearch;
    }

    /**
     * Set the tag search object
     *
     * @param tagSearch Tag search object
     */
    public void setTagSearch(TagSearch<P> tagSearch) {
        this.tagSearch = tagSearch;
    }

    /**
     * Get the search requester
     *
     * @return Search requester
     */
    public R getRequester() {
        return requester;
    }

    /**
     * Set the search requester
     *
     * @param requester Search requester
     */
    public void setRequester(R requester) {
        this.requester = requester;
    }

    /**
     * Get the search data provider
     *
     * @return Search data provider
     */
    public ArrayList<P> getProvider() {
        return provider;
    }

    /**
     * Set the search data provider
     *
     * @param provider Search data provider
     */
    public void setProvider(ArrayList<P> provider) {
        this.provider = provider;
    }

    /**
     * Sort the results in descending order of match percentage
     */
    public void sort() {
        results.sort(new SearchResultComparator<P>());
    }

    public abstract void go();
    public abstract void go(String searchTerms, String separator);
    public abstract void go(ArrayList<String> categories,
                            String searchTerms,
                            String separator,
                            int payFrom,
                            int payTo);
}

package com.monash.model.Search;

/**
 * Class for storing a match result
 * @author ITO5136-TP6-21 Team D
 * @version 0.1
 */
public class SearchResult<T> implements Comparable<SearchResult<T>> {
    private T entity;
    private int result;

    /**
     * Default constructor
     */
    public SearchResult() {
        entity = null;
        result = 0;
    }

    /**
     * Constructor
     * @param entity Result entity
     * @param result Result
     */
    public SearchResult(T entity, int result) {
        this.entity = entity;
        this.result = result;
    }

    /**
     * Get the matched entity
     * @return Matched entity
     */
    public T getEntity() {
        return entity;
    }

    /**
     * Get the match result
     * @return Match result
     */
    public int getResult() {
        return result;
    }

    /**
     * Set the matched entity
     * @param entity Matched entity
     */
    public void setEntity(T entity) {
        this.entity = entity;
    }


    /**
     * Set the match result value
     * @param result Match result value
     */
    public void setResult(int result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "entity=" + entity.toString() +
                ", result=" + result +
                '}';
    }

    @Override
    public int compareTo(SearchResult<T> o) {
        return o.getResult() - this.getResult();
    }
}
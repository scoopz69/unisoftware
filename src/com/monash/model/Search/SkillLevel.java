package com.monash.model.Search;

/**
 * Skill levels
 * @author   ITO5136-TP6-2021 Team D
 * @version  0.0.1
 */
public enum SkillLevel {
    NOVICE("Novice", 1),
    INTERMEDIATE("Intermediate", 2),
    ADVANCED("Advanced", 3),
    EXPERT("Expert", 4);

    private int level;
    private String title;

    SkillLevel(String title, int level) {
        this.title = title;
        this.level = level;
    }

    public int level() { return level; }
    public String title() { return title; }
}

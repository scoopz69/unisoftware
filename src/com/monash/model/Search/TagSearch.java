package com.monash.model.Search;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Search class
 * @author ITO5136-TP6-2021 Team D
 * @version 0.1
 */
public class TagSearch<R> {
    private ArrayList<SearchResult<R>> results;
    private ArrayList<TagList<R>> searchTags;
    private TagList targetTags;

    /**
     * Default constructor
     */
    public TagSearch() {
        results = new ArrayList<SearchResult<R>>();
        searchTags = new ArrayList<TagList<R>>();
        targetTags = new TagList();
    }

    /**
     * Constructor
     * @param searchTags Arraylist of TagLists to be searched
     * @param targetTags Taglist to be searched for
     */
    public TagSearch(ArrayList<TagList<R>> searchTags, TagList targetTags) {
        this();
        this.searchTags = searchTags;
        this.targetTags = targetTags;
    }

    /**
     * Get the search results in descending order of match percentage
     * @return Search results
     */
    public ArrayList<SearchResult<R>> getResults() {
        Collections.sort(results);
        return results;
    }

    /**
     * Get the tags to be searched
     * @return Tags to be searched
     */
    public ArrayList<TagList<R>> getSearchTags() {
        return searchTags;
    }

    /**
     * Get the tags to be searched for
     * @return Tags to be searched for
     */
    public TagList getTargetTags() {
        return targetTags;
    }

    /**
     * Set the tags to be searched
     * @param searchTags Tags to be searched
     */
    public void setSearchTags(ArrayList<TagList<R>> searchTags) {
        this.searchTags = searchTags;
    }

    /**
     * Set the tags to be searched for
     * @param targetTags Tags to be searched for
     */
    public void setTargetTags(TagList targetTags) {
        this.targetTags = targetTags;
    }

    /**
     * Execute the search
     * @return Number of results
     */
    public void go() {
        for (TagList<R> tags : searchTags) {
            int matchPercent = tags.compare(targetTags);

            if (matchPercent > 0)
                results.add(new SearchResult<R>(tags.getOwner(), matchPercent));
        }
    }
}

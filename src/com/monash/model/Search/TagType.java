package com.monash.model.Search;

/**
 * Tag types
 * @author   ITO5136-TP6-2021 Team D
 * @version  0.0.1
 */
public enum TagType {
    SKILL("SKILL"),
    QUALIFICATION("QUALIFICATION");

    private String title;

    TagType(String title) { this.title = title; }
    public String title() { return title; }
}

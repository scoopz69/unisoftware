package com.monash.model.Search;

import com.monash.View.JobForm;
import com.monash.model.*;

import java.util.ArrayList;

/**
 * Job searcher: seekers to jobs
 *
 * @author   ITO5136-TP6-2021 Team D
 * @version  0.0.1
 */
public class SeekerSearch extends Search<User, Job> {
    /**
     * Default constructor
     */
    public SeekerSearch() {
        super();
    }

    /**
     * Constructor
     *
     * @param user User object
     * @param jobs Jobs list
     */
    public SeekerSearch(User user, ArrayList<Job> jobs) {
        super(user, jobs);
    }

    @Override
    public void go() {}

    @Override
    public void go(String searchTerms, String separator) {}

    @Override
    public void go(ArrayList<String> categories, String terms, String sep, int payFrom, int payTo) {
        ArrayList<TagList<Job>> searchBase = new ArrayList<TagList<Job>>();
        TagList<String> catTags = buildCategoryTagList(categories);
        TextSearch textSearch = new TextSearch();

        // Filter out jobs that fail a salary range check
        for (Job job : provider) {
            if (checkSalaryRange(payFrom, payTo, job.getPayLow(), job.getPayHigh()))
                searchBase.add(new TagList<Job>(job.getJobTags().getTags(), job));
        }

        // Do a category search if required
        if (catTags.count() > 0) {
            TagSearch<Job> tagSearch = new TagSearch<Job>(searchBase, catTags);
            tagSearch.go();

            ArrayList<SearchResult<Job>> tagSearchResults = tagSearch.getResults();

            if (!tagSearchResults.isEmpty()) {
                for (SearchResult<Job> result : tagSearchResults) {
                    // Do a text search if required
                    if (!terms.isEmpty() && !terms.isBlank()) {
                        textSearch.setTextA(terms);
                        textSearch.setTextB(result.getEntity().searchText());
                        textSearch.setSeparator(sep);
                        textSearch.go();

                        // Only keep non-zero results for both searches
                        if (textSearch.getMatchPercent() > 0) {
                            int match = (textSearch.getMatchPercent() + result.getResult()) / 2;
                            results.add(new SearchResult<Job>(result.getEntity(), match));
                        }
                    }
                    else {
                        // No text search required: keep tag search result
                        results.add(new SearchResult<Job>(result.getEntity(), result.getResult()));
                    }
                }
            }
        }
        else if (!terms.isBlank() && !terms.isEmpty()) {
            // Do a text search
            for (TagList<Job> jobTags : searchBase) {
                textSearch.setTextA(terms);
                textSearch.setTextB(jobTags.getOwner().searchText());
                textSearch.setSeparator(sep);
                textSearch.go();

                if (textSearch.getMatchPercent() > 0) {
                    results.add(new SearchResult<Job>(jobTags.getOwner(), textSearch.getMatchPercent()));
                }
            }
        }
        else {
            // Keep all jobs that pass the salary check
            for (TagList<Job> result : searchBase)
                results.add(new SearchResult<Job>(result.getOwner(), 100));
        }
    }

    /**
     * Perform a salary range check
     *
     * @param seekMinSalary Minimum sought salary (0 = no minimum)
     * @param seekMaxSalary Maximum sought salary (0 = no maximum)
     * @param jobMinSalary  Minimum job salary
     * @param jobMaxSalary  Maximum job salary
     * @return True if successful, false otherwise
     */
    public boolean checkSalaryRange(int seekMinSalary,
                                    int seekMaxSalary,
                                    int jobMinSalary,
                                    int jobMaxSalary) {
        boolean minPass = false;
        boolean maxPass = false;

        if (seekMinSalary <= 0) {
            minPass = true;
        } else if (seekMinSalary <= jobMinSalary) {
            minPass = true;
        }

        if (seekMaxSalary <= 0) {
            maxPass = true;
        } else if (seekMaxSalary <= jobMaxSalary) {
            maxPass = true;
        }

        return minPass && maxPass;
    }

    /**
     * Build a category tag list
     *
     * @param categories Category tag list
     * @return
     */
    private TagList<String> buildCategoryTagList(ArrayList<String> categories) {
        TagList<String> categoryTags = new TagList<String>();

        for (String category : categories) {
            TagKey key = new TagKey("CATEGORY", category, "");
            Tag tag = new Tag(0, key, 0);
            categoryTags.add(tag);
        }

        return categoryTags;
    }

    /**
     * Helper method for building an ArrayList of category TagLists for all jobs
     *
     * @param jobs ArrayList of jobs
     * @return ArrayList of category TagLists
     */
    public ArrayList<TagList<Job>> getCategoryTagLists(ArrayList<Job> jobs) {
        ArrayList<TagList<Job>> jobTagLists = new ArrayList<TagList<Job>>();

        for (Job job : jobs)
            jobTagLists.add(job.getJobTags("CATEGORY"));

        return jobTagLists;
    }
}

package com.monash.model.Search;

import com.monash.model.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;

/**
 * Job searcher: match jobs to seekers
 *
 * @author   ITO5136-TP6-2021 Team D
 * @version  0.0.1
 */
public class JobSearch extends Search<Job, User> {
    /**
     * Default constructor
     */
    public JobSearch() {
        super();
    }

    /**
     * Constructor
     *
     * @param job   Search requester
     * @param users Search data provider
     */
    public JobSearch(Job job, ArrayList<User> users) {
        super(job, users);
    }

    @Override
    public void go() {
        tagSearch.setTargetTags(requester.getJobTags("SKILL"));
        tagSearch.setSearchTags(getSkillTagLists(provider));
        tagSearch.go();

        results = tagSearch.getResults();
    }

    @Override
    public void go(String searchTerms, String separator) {
        HashMap<TagKey, Tag> jobTags = new HashMap<TagKey, Tag>();
        String[] skills = searchTerms.split(separator);

        for (int i = 0; i < skills.length; i++) {
            TagKey key = new TagKey("SKILL", skills[i].toUpperCase(Locale.ROOT), "");
            Tag tag = new Tag(i + 1, key);
            jobTags.putIfAbsent(key, tag);
        }

        TagList jobTagList = new TagList(jobTags, requester);
        tagSearch.setTargetTags(jobTagList);
        tagSearch.setSearchTags(getSkillTagLists(provider));
        tagSearch.go();
    }

    @Override
    public void go(ArrayList<String> categories,
                   String searchTerms,
                   String separator,
                   int payFrom,
                   int payTo) {}

    /**
     * Helper method for extracting all the tags for a list of users
     *
     * @param users ArrayList of users
     * @return An ArrayList of user TagLists
     */
    public ArrayList<TagList<User>> getTagLists(ArrayList<User> users) {
        ArrayList<TagList<User>> userTagLists = new ArrayList<TagList<User>>();

        for (User user : users)
            userTagLists.add(user.getAllTags());

        return userTagLists;
    }

    /**
     * Helper method for extracting all the skill tags for a list of users
     *
     * @param users ArrayList of users
     * @return An ArrayList of user TagLists
     */
    public ArrayList<TagList<User>> getSkillTagLists(ArrayList<User> users) {
        ArrayList<TagList<User>> userTagLists = new ArrayList<TagList<User>>();

        for (User user : users)
            userTagLists.add(user.getSkillTags());

        return userTagLists;
    }

    /**
     * Helper method for extracting all the qualification tags for a list of users
     *
     * @param users ArrayList of users
     * @return An ArrayList of user TagLists
     */
    public static ArrayList<TagList<User>> getQualificationTagLists(ArrayList<User> users) {
        ArrayList<TagList<User>> userTagLists = new ArrayList<TagList<User>>();

        for (User user : users)
            userTagLists.add(user.getQualificationTags());

        return userTagLists;
    }
}

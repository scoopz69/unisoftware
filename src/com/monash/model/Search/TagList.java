package com.monash.model.Search;

import com.monash.model.Tag;
import com.monash.model.TagKey;

import java.util.*;

/**
 * Tag list class
 * @author   ITO5136-TP6-2021 Team D
 * @version  0.0.1
 */
public class TagList<T> {
    private HashMap<TagKey, Tag> tags;
    private T owner;

    /**
     * Default constructor
     */
    public TagList() {
        tags = new HashMap<TagKey, Tag>();
        owner = null;
    }

    /**
     * Constructor
     * @param tags  HashMap of tags
     * @param owner TagList owner
     */
    public TagList(HashMap<TagKey, Tag> tags, T owner) {
        this();
        this.tags = tags;
        this.owner = owner;
    }

    /**
     * Get the tag list owner
     * @return Owner object
     */
    public T getOwner() {
        return owner;
    }

    /**
     * Get the tags
     * @return Tags
     */
    public HashMap<TagKey, Tag> getTags() {
        return tags;
    }

    /**
     * Set the tag list owner
     * @param owner Owner object
     */
    public void setOwner(T owner) {
        this.owner = owner;
    }

    /**
     * Set the tags
     * @param tags Tags
     */
    public void setTags(HashMap<TagKey, Tag> tags) {
        this.tags = tags;
    }

    /**
     * Add a tag to the list
     * @param tag Tag to be added
     * @return True if successful, false otherwise
     */
    public boolean add(Tag tag) {
        return tags.putIfAbsent(tag.getKey(), tag) == null ? false : true;
    }

    /**
     * Compare this tag list to another
     * @param compTags Tag list for comparison
     * @return Percentage match between the two lists
     */
    public int compare(TagList compTags) {
        int matchPoints = 0;

        HashMap<TagKey, Tag> mapBig = new HashMap<TagKey, Tag>();
        HashMap<TagKey, Tag> mapSml = new HashMap<TagKey, Tag>();

        if (compTags.count() > count()) {
            mapBig = compTags.getTags();
            mapSml = tags;
        }
        else {
            mapSml = compTags.getTags();
            mapBig = tags;
        }

        for (Map.Entry<TagKey, Tag> tagEntry : mapSml.entrySet()) {
            if (mapBig.containsKey(tagEntry.getKey()))
                matchPoints++;
        }

        double maxPoints = mapSml.size();
        double percent = maxPoints > 0 ? (double)matchPoints / maxPoints : 0;
        return (int)(percent * 100);
    }

    /**
     * Check if a tag is contained in the list
     * @param tag Tag to look for
     * @return True if found, false otherwise
     */
    public boolean contains(Tag tag) {
        return tags.containsKey(tag.getKey());
    }

    /**
     * Check if a tag with the given key is contained in the list
     * @param key Tag key to look for
     * @return True if found, false otherwise
     */
    public boolean contains(TagKey key) {
        return tags.containsKey(key);
    }

    /**
     * Check if a tag with the given type and name is contained in the list
     * @param type Tag type to look for
     * @param name Tag name to look for
     * @return True if found, false otherwise
     */
    public boolean contains(String type, String name) {
        boolean result = false;

        for (Map.Entry<TagKey, Tag> tagEntry : tags.entrySet()) {
            if (tagEntry.getKey().getType().equalsIgnoreCase(type) &&
                tagEntry.getKey().getName().equalsIgnoreCase(name)) {
                result = true;
                break;
            }
        }

        return result;
    }

    /**
     * Check if a tag with the given type is contained in the list
     * @param type Tag type to look for
     * @return True if found, false otherwise
     */
    public boolean contains(String type) {
        boolean result = false;

        for (Map.Entry<TagKey, Tag> tagEntry : tags.entrySet()) {
            if (tagEntry.getKey().getType().equalsIgnoreCase(type)) {
                result = true;
                break;
            }
        }

        return result;
    }

    /**
     * Get the number of tags in the list
     * @return Number of tags in the list
     */
    public int count() {
        return tags.size();
    }

    /**
     * Remove a tag from the list
     * @param tag Tag to be removed
     * @return True if successful, false otherwise
     */
    public boolean remove(Tag tag) {
        return tags.remove(tag.getKey()) != null;
    }

    /**
     * Return the tags as an ArrayList
     * @return ArrayList of tags
     */
    public ArrayList<Tag> toArrayList() {
        ArrayList<Tag> results = new ArrayList<Tag>();

        for (Map.Entry<TagKey, Tag> tagEntry : tags.entrySet()) {
            results.add(tagEntry.getValue());
        }

        return results;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TagList<?> tagList = (TagList<?>) o;
        return Objects.equals(tags, tagList.tags) && Objects.equals(owner, tagList.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tags, owner);
    }
}
package com.monash.model.Search;

import com.monash.model.Tag;
import com.monash.model.TagKey;
import com.monash.model.Util;

import java.util.ArrayList;

/**
 * Text search
 *
 * @author   ITO5136-TP6-2021 Team D
 * @version  0.0.1
 */
public class TextSearch {
    private String textA;
    private String textB;
    private String separator;
    private int matchPercent;

    /**
     * Default constructor
     */
    public TextSearch() {
        textA = "";
        textB = "";
        separator = " ";
        matchPercent = 0;
    }

    /**
     * Constructor
     *
     * @param textA Text A
     * @param textB Text B
     */
    public TextSearch(String textA, String textB) {
        this();
        setTextA(textA);
        setTextB(textB);
    }

    /**
     * Constructor
     *
     * @param textA     Text A
     * @param textB     Text B
     * @param separator Term separator
     */
    public TextSearch(String textA, String textB, String separator) {
        this(textA, textB);
        this.separator = separator;
    }

    /**
     * Get the overall match percentage
     *
     * @return Overall match percentage
     */
    public int getMatchPercent() {
        return matchPercent;
    }

    /**
     * Get text A
     *
     * @return text A
     */
    public String getTextA() {
        return textA;
    }

    /**
     * Get the term separator
     *
     * @return Term separator
     */
    public String getSeparator() {
        return separator;
    }

    /**
     * Get text B
     *
     * @return Text B
     */
    public String getTextB() {
        return textB;
    }

    /**
     * Set text A
     * @param textA Text A
     */
    public void setTextA(String textA) {
        this.textA = Util.stripPunctuation(textA);
    }

    /**
     * Set the term separator
     *
     * @param separator Term separator
     */
    public void setSeparator(String separator) {
        this.separator = separator;
    }

    /**
     * Set text B
     *
     * @param textB Text B
     */
    public void setTextB(String textB) {
        this.textB = Util.stripPunctuation(textB);
    }

    /**
     * Execute the search
     */
    public void go() {
        String[] textAWords = textA.split(separator);
        String[] textBWords = textB.split(separator);

        TagList<String> textATags = new TagList<String>();
        TagList<String> textBTags = new TagList<String>();

        textATags.setOwner("TEXT-A");
        for (int i = 0; i < textAWords.length; i ++) {
            Tag tag = new Tag(i + 1, new TagKey(textAWords[i], textAWords[i], textAWords[i]));
            textATags.add(tag);
        }

        textBTags.setOwner("TEXT-B");
        for (int i = 0; i < textBWords.length; i++) {
            Tag tag = new Tag(i + 1, new TagKey(textBWords[i], textBWords[i], textBWords[i]));
            textBTags.add(tag);
        }

        ArrayList<TagList<String>> sourceTagBase = new ArrayList<TagList<String>>();
        TagSearch<String> tagSearch = new TagSearch<String>();

        if (textATags.count() >= textBTags.count()) {
            sourceTagBase.add(textATags);
            tagSearch.setTargetTags(textBTags);
        }
        else {
            sourceTagBase.add(textBTags);
            tagSearch.setTargetTags(textATags);
        }

        tagSearch.setSearchTags(sourceTagBase);

        tagSearch.go();
        ArrayList<SearchResult<String>> results = tagSearch.getResults();
        matchPercent = results.size() > 0 ? results.get(0).getResult() : 0;
    }

    @Override
    public String toString() {
        return "TextSearch{" +
                "textA='" + textA + '\'' +
                ", textB='" + textB + '\'' +
                ", separator='" + separator + '\'' +
                ", matchPercent=" + matchPercent +
                '}';
    }
}

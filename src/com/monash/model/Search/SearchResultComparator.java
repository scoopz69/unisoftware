package com.monash.model.Search;

import java.util.Comparator;

/**
 * Comparator for sorting of search results in descending order of match percentage
 * @author ITO5136-TP6-21 Team D
 * @version 0.1
 */
public class SearchResultComparator<T> implements Comparator<SearchResult<T>> {
    @Override
    public int compare(SearchResult<T> o1, SearchResult<T> o2) {
        return o2.getResult() - o1.getResult();
    }
}

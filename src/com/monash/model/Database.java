package com.monash.model;

import com.monash.model.Search.TagType;

import java.sql.*;
import java.util.*;
import java.util.Date;

//TODO add refresh methods for message + job lists
public class Database {

    //TODO remove redudant storage lists
    private ArrayList<User> userList;
    private Connection con;
    private ArrayList<Job> jobsList;
    private ArrayList<Message> messageList;
    private ArrayList<Job> searchResults;
    private ArrayList<Tag> tagsList;
    private HashMap<Integer, String> categories;

    public Database() throws Exception {

        userList = new ArrayList<User>();
        jobsList = new ArrayList<Job>();
        messageList = new ArrayList<Message>();
        searchResults = new ArrayList<Job>();
        tagsList = new ArrayList<Tag>();
        categories = new HashMap<Integer, String>();

        connect();
        Connection con;
    }

    //PRODUCTION DATABASE AWS MYSQL  connection
    public void connect() throws Exception {

        if (con != null)
            return;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new Exception("Driver not found");
        }

        String url = "jdbc:mysql://monash.c6fdgof7ctia.us-east-2.rds.amazonaws.com";
        con = DriverManager.getConnection(url, "root", "password");
        System.out.println("Database AWS connection successful");
    }
    //FINAL uncomment for production DB

   /* //lOCAL HOST DATABASE MYSQL WORKBENCH // ----------------------------------------------

    public void connect() throws Exception {

        if (con != null)
            return;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new Exception("Driver not found");
        }


        //MAKE DATABASE "test" on local mysql server
        String url = "jdbc:mysql://localhost:3306/monashjss";

        //SET YOUR MYSQL user + password where below has root and password
        con = DriverManager.getConnection(url, "root", "password");
        System.out.println("Database localhost connection successful");
    }
    //END OF LOCAL TEST DATABASE//--------------------------------------------*/

    public void disconnect() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println("Can't close connection");
            }
        }
    }


    ///STARTUP user specific data loads//////
    public void loadStartData() throws SQLException {
        dbLoadCategories();
        loadUsers();
    }

    public void loadRecruiterData(int recruiterId) throws SQLException {
        //TODO reduce down to joblist and SQL where recruiter_id
        loadRecruiterJobs(recruiterId);
        setUserSkillQuali();
        setJobSkills();
    }

    public void loadSeekerData() throws SQLException {
        try {
            loadJobs();
            setUserSkillQuali();
            setJobSkills();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Clears locally stored data on exit//
    public void clearData() {
        jobsList.clear();
        messageList.clear();
        searchResults.clear();
        tagsList.clear();
    }

    //// START LOAD USERS METHODS  /////--------------------------------------------
    public void loadUsers() throws SQLException {
        String sql = "SELECT * FROM `monashjss`.`users` ORDER BY id ASC;\n";
        Statement selectStatement = con.createStatement();
        ResultSet results = selectStatement.executeQuery(sql);

        while (results.next()) {
            int id = results.getInt("id");
            String password = results.getString("password");
            String firstName = results.getString("firstName");
            String lastName = results.getString("lastName");
            String email = results.getString("email");
            Boolean isRecruiter = results.getBoolean("isRecruiter");
            String city = results.getString("city");

            User user = new User(id, password, firstName, lastName, email, isRecruiter, 0, city);
            userList.add(user);
        }
        System.out.println("userlist loaded successfully");
        results.close();
        selectStatement.close();
    }

    public ArrayList<User> getUserList() {
        Collections.sort(userList);
        return userList;
    }

    public ArrayList<Tag> getTagsList() {
        return tagsList;
    }

    public HashMap<Integer, String> dbGetCategories() throws SQLException {
        dbLoadCategories();
        return categories;
    }

    public void loadJobs() throws SQLException {
        try {
            String sql = "SELECT * FROM `monashjss`.`jobs`;\n";
            Statement selectStatement = con.createStatement();
            ResultSet results = selectStatement.executeQuery(sql);

            while (results.next()) {
                int jobId = results.getInt("job_id");
                String summary = results.getString("summary");
                Date date = results.getDate("date");
                String title = results.getString("title");
                String company = results.getString("company");
                String location = results.getString("location");
                String description = results.getString("description");
                int payLow = results.getInt("pay_low");
                int payHigh = results.getInt("pay_high");
                int tagListId = results.getInt("tag_list_id");
                boolean active = results.getBoolean("active");
                int recruiterId = results.getInt("recruiter_id");

                Job job = new Job(jobId, summary, date, title, company, location, description,
                        payLow, payHigh, tagListId, active, recruiterId);
                jobsList.add(job);
            }
            System.out.println("Seeker jobs list loaded successfully ");
            results.close();
            selectStatement.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //TODO reduce double load for recruiter jobs
    public void loadRecruiterJobs(int recId) throws SQLException {
        try {
            PreparedStatement statement = con.prepareStatement("SELECT * FROM monashjss.jobs WHERE recruiter_id = ?");
            statement.setInt(1, recId);
            ResultSet results = statement.executeQuery();

            while (results.next()) {
                int jobId = results.getInt("job_id");
                String summary = results.getString("summary");
                Date date = results.getDate("date");
                String title = results.getString("title");
                String company = results.getString("company");
                String location = results.getString("location");
                String description = results.getString("description");
                int payLow = results.getInt("pay_low");
                int payHigh = results.getInt("pay_high");
                int tagListId = results.getInt("tag_list_id");
                boolean active = results.getBoolean("active");
                int recruiterId = results.getInt("recruiter_id");

                Job job = new Job(jobId, summary, date, title, company, location, description,
                        payLow, payHigh, tagListId, active, recruiterId);
                jobsList.add(job);
            }
            System.out.println("Recruiter jobs list loaded successfully ");
            results.close();
            statement.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public ArrayList<Job> getJobsList() {
        Collections.sort(jobsList);
        return jobsList;
    }

    public void dbAddJob(Job job) throws SQLException {

        String summary = job.getSummary();
        String title = job.getTitle();
        String company = job.getCompany();
        String location = job.getLocation();
        String description = job.getDescription();
        int payLow = job.getPayLow();
        int payHigh = job.getPayHigh();
        int tagListId = job.getTagListId();
        boolean active = job.isActive();
        int recruiterId = job.getRecruiterId();
        ArrayList<Tag> tagsList = job.getJobTagList();

        try {
            String insertSql = "insert into monashjss.jobs" +
                    " (summary, date, title, company, location, description, pay_low, pay_high, tag_list_id, active, recruiter_id)" +
                    " values (?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement insertStatement = con.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
            Date date = new Date();
            java.sql.Date sqlDate = new java.sql.Date(date.getTime());

            int col = 1;
            insertStatement.setString(col++, summary);
            insertStatement.setDate(col++, sqlDate);
            insertStatement.setString(col++, title);
            insertStatement.setString(col++, company);
            insertStatement.setString(col++, location);
            insertStatement.setString(col++, description);
            insertStatement.setInt(col++, payLow);
            insertStatement.setInt(col++, payHigh);
            insertStatement.setInt(col++, tagListId);
            insertStatement.setBoolean(col++, active);
            insertStatement.setInt(col++, recruiterId);
            insertStatement.executeUpdate();

            int newId = 0;
            ResultSet keys = insertStatement.getGeneratedKeys();
            keys.next();
            newId = keys.getInt(1);
            insertStatement.close();
            dbUpdateJobSkill(newId, tagsList);
            //adds the job to the local session
            jobsList.add(job);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void dbUpdateJob(Job job) throws SQLException {

        int jobId = job.getJobId();
        String summary = job.getSummary();
        String title = job.getTitle();
        String company = job.getCompany();
        String location = job.getLocation();
        String description = job.getDescription();
        int payLow = job.getPayLow();
        int payHigh = job.getPayHigh();
        boolean active = job.isActive();
        ArrayList<Tag> tagList = job.getJobTagList();

        try {
            String insertSql = "UPDATE monashjss.jobs SET summary = ?, title = ?, company=?, " +
                    "location=?, description=?, pay_low=?, pay_high=?, active=? WHERE job_id = ? ";
            PreparedStatement updateStatement = con.prepareStatement(insertSql);

            int col = 1;
            updateStatement.setString(col++, summary);
            updateStatement.setString(col++, title);
            updateStatement.setString(col++, company);
            updateStatement.setString(col++, location);
            updateStatement.setString(col++, description);
            updateStatement.setInt(col++, payLow);
            updateStatement.setInt(col++, payHigh);
            updateStatement.setBoolean(col++, active);
            updateStatement.setInt(col++, jobId);
            updateStatement.executeUpdate();
            updateStatement.close();
            dbUpdateJobSkill(jobId, tagList);
            deleteLocalJob(jobId);
            jobsList.add(job);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("job update successfull");
    }

    private void dbUpdateJobCategory(int jobId, ArrayList<Tag> categoryList) throws SQLException {
        dbDeleteJobCategory(jobId);
        String insertSql = "INSERT INTO monashjss.tags(job_id, type, name) VALUES (?,?,?)";
        PreparedStatement insertStatement = con.prepareStatement(insertSql);

        for (int i = 0; i < categoryList.size(); i++) {
            Tag tempTag = categoryList.get(i);
            String name = tempTag.getKey().getName();

            int col = 1;
            insertStatement.setInt(col++, jobId);
            insertStatement.setString(col++, "CATEGORY");
            insertStatement.setString(col++, name);
            insertStatement.addBatch();
            System.out.println("CATEGORY updated:  " + name);
        }
        insertStatement.executeBatch();
        insertStatement.close();
        System.out.println("job CATEGORY update successful");
    }

    public void dbDeleteJobCategory(int jobId) throws SQLException {
        String sql = "DELETE FROM monashjss.tags WHERE job_id = ? AND type = 'CATEGORY'";
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, jobId);
        ps.executeUpdate();
        ps.close();
    }

    private void deleteLocalJob(int jobId) {
        int jobIdCheck = jobId;
        for (int i = 0; i < jobsList.size(); i++) {
            int jobIdFound = jobsList.get(i).getJobId();
            if (jobIdCheck == jobIdFound) {
                jobsList.remove(i);
                System.out.println("Job deleted at: " + i);
                break;
            }
            System.out.println("job delete loop finished");
        }
    }

    public String databaseAddUser(String password, String firstName, String lastName, String email, Boolean isRecruiter) throws SQLException {
        try {
            String insertSql = "insert into monashjss.users (password,firstName,lastName, email, isRecruiter ) values (?,?,?,?,?)";
            PreparedStatement insertStatement = con.prepareStatement(insertSql);
            int col = 1;
            insertStatement.setString(col++, password);
            insertStatement.setString(col++, firstName);
            insertStatement.setString(col++, lastName);
            insertStatement.setString(col++, email);
            insertStatement.setBoolean(col++, isRecruiter);
            insertStatement.executeUpdate();
            insertStatement.close();

            /*if(!isRecruiter) {
                int tempId = dbGetUserIdEmail(email);
                reserveSkillDb(tempId);
            }*/

            loadUsers();
            return "Registration successful";

        } catch (Exception e) {
            return "Error, job not updated. please check inputs";
        }
    }

    public String dbUpdateUserProfile(int userId, String firstName, String lastName, String email) throws SQLException {
        try {
            String insertSql = "UPDATE monashjss.users SET firstName = ?, lastName = ?, email = ? WHERE id = ?";
            PreparedStatement insertStatement = con.prepareStatement(insertSql);
            int col = 1;
            insertStatement.setString(col++, firstName);
            insertStatement.setString(col++, lastName);
            insertStatement.setString(col++, email);
            insertStatement.setInt(col++, userId);

            insertStatement.executeUpdate();
            insertStatement.close();
            //TODO recheck refresh
            loadUsers();

            return "Profile update successful";
        } catch (Exception e) {
            return "Error, profile not updated";
        }
    }

    public String dbUpdatePassword(int userId, String password) throws SQLException {
        try {
            String insertSql = "UPDATE monashjss.users SET password = ? WHERE id = ?";
            PreparedStatement insertStatement = con.prepareStatement(insertSql);
            int col = 1;
            insertStatement.setString(col++, password);
            insertStatement.setInt(col++, userId);
            insertStatement.executeUpdate();
            insertStatement.close();
            //TODO recheck refresh
            loadUsers();

            return "Password update successful";
        } catch (Exception e) {
            return "Error, password not updated";
        }
    }

    public void dbUpdateJobSkill(int jobId, ArrayList<Tag> tagList) throws SQLException {
        dbDeleteJobSkill(jobId);
        String insertSql = "INSERT INTO monashjss.tags(job_id, type, name) VALUES (?,?,?)";
        PreparedStatement insertStatement = con.prepareStatement(insertSql);

        System.out.println("Outer loop test name: " + tagList.get(0).getKey().getName());
        for (int i = 0; i < tagList.size(); i++) {
            Tag tempTag = tagList.get(i);
            String name = tempTag.getKey().getName();

            int col = 1;
            insertStatement.setInt(col++, jobId);
            insertStatement.setString(col++, tagList.get(i).getKey().getType());
            insertStatement.setString(col++, name);
            insertStatement.addBatch();
            System.out.println("job id + tag name " + jobId + " " + name);
        }
        insertStatement.executeBatch();
        insertStatement.close();
        System.out.println("job skill update successful");
    }

    public void dbUpdateSeekerSkill(int userId, ArrayList<Tag> tagList) throws SQLException {
        dbDeleteUserSkill(userId);
        String insertSql = "INSERT INTO monashjss.tags(user_id, type, name, value) VALUES (?,?,?,?)";
        PreparedStatement insertStatement = con.prepareStatement(insertSql);

        for (int i = 0; i <= 10; i++) {
            Tag tempTag = tagList.get(i);
            String name = tempTag.getKey().getName();
            String value = tempTag.getKey().getValue();

            int col = 1;
            insertStatement.setInt(col++, userId);
            insertStatement.setString(col++, "SKILL");
            insertStatement.setString(col++, name);
            insertStatement.setString(col++, value);
            insertStatement.executeUpdate();
            System.out.println("skill update successful");
        }
        //insertStatement.executeUpdate();
        insertStatement.close();
    }

    public void setUserSkillQuali() throws SQLException {
        setUserTags();
    }

    public void setJobSkills() throws SQLException {
        ArrayList<Tag> jobSkillTagList = dbGetJobTags("SKILL");
        ArrayList<Tag> jobCategoryTagList = dbGetJobTags("CATEGORY");

        for (Job job : jobsList) {
            ArrayList<Tag> jobTagList = new ArrayList<Tag>();

            for (Tag tag : jobSkillTagList) {
                if (job.getJobId() == tag.getUserId())
                    jobTagList.add(tag);
            }

            for (Tag tag : jobCategoryTagList) {
                if (job.getJobId() == tag.getUserId())
                    jobTagList.add(tag);
            }

            job.setJobTagList(jobTagList);
        }

        System.out.println("Job tags added to jobs");
    }

    public ArrayList<Tag> dbGetJobTags(String tagType) throws SQLException {
        ArrayList<Tag> jobTagsList = new ArrayList<Tag>();
        String sql = "SELECT * FROM `monashjss`.`tags` WHERE job_id > 0 AND type = '" + tagType +
                "' ORDER BY tag_id ASC";

        PreparedStatement ps = con.prepareStatement(sql);
        ResultSet results = ps.executeQuery();

        while (results.next()) {
            int id = results.getInt("tag_id");
            int jobId = results.getInt("job_id");
            String type = results.getString("type");
            String name = results.getString("name");
            String value = results.getString("value");

            TagKey tempKey = new TagKey(type, name, value);
            Tag tempTag = new Tag(id, tempKey, jobId);

            jobTagsList.add(tempTag);
        }

        return jobTagsList;
    }

    private ArrayList<Tag> dbGetUserTags() throws SQLException {
        ArrayList<Tag> userTags = new ArrayList<Tag>();
        String sql = "SELECT * FROM `monashjss`.`tags` WHERE user_id > 0 ORDER BY tag_id ASC";
        PreparedStatement ps = con.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            TagKey key = new TagKey(rs.getString("type"),
                    rs.getString("name"),
                    rs.getString("value"));

            Tag tag = new Tag(rs.getInt("tag_id"), key, rs.getInt("user_id"));

            userTags.add(tag);
        }

        return userTags;
    }

    public void setUserTags() throws SQLException {
        ArrayList<Tag> skillTagList = dbGetUserTags();

        for (User user : userList) {
            ArrayList<Tag> skillTags = new ArrayList<Tag>();
            ArrayList<Tag> qualfTags = new ArrayList<Tag>();

            for (Tag tag : skillTagList) {
                if (user.getId() == tag.getUserId()) {
                    switch (tag.getKey().getType()) {
                        case "SKILL":
                            skillTags.add(tag);
                            break;
                        case "QUALIFICATION":
                            qualfTags.add(tag);
                            break;
                        default:
                            break;
                    }
                }
            }
            user.setSkillTagList(skillTags);
            user.setQualifTagList(qualfTags);
        }
        System.out.println("User tags added to users");
    }

    public void dbDeleteUserSkill(int id) throws SQLException {
        String sql = "  DELETE FROM `monashjss`.`tags`\n" +
                "        WHERE user_id = ? AND type = 'SKILL';\n";

        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, id);
        ps.executeUpdate();
        ps.close();
    }

    private void dbDeleteJobSkill(int id) throws SQLException {
        String sql = "  DELETE FROM `monashjss`.`tags`\n" +
                "        WHERE job_id = ?;\n";

        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, id);
        ps.executeUpdate();
        ps.close();
    }

    public String dbAddMessage(String sender, String roleName, String messageContent, boolean read, int senderId, int receiverId) throws SQLException {
        try {
            String insertSql = "INSERT INTO `monashjss`.`messages`\n" +
                    "            (" +
                    "            `sender`,\n" +
                    "            `date`,\n" +
                    "            `role_name`,\n" +
                    "            `message_content`,\n" +
                    "            `read`,\n" +
                    "            `sender_id`,\n" +
                    "            `receiver_id`) values(?,?,?,?,?,?,?)";
            PreparedStatement insertStatement = con.prepareStatement(insertSql);
            Date date = new Date();
            java.sql.Date sqldate = new java.sql.Date(date.getTime());

            int col = 1;
            insertStatement.setString(col++, sender);
            insertStatement.setDate(col++, sqldate);
            insertStatement.setString(col++, roleName);
            insertStatement.setString(col++, messageContent);
            insertStatement.setBoolean(col++, read);
            insertStatement.setInt(col++, senderId);
            insertStatement.setInt(col++, receiverId);

            insertStatement.executeUpdate();
            insertStatement.close();
            return "Message sent successfully";
        } catch (Exception e) {
            return "Error, Message not sent";
        }
    }

    public void dbLoadSeekerMessages(int seekerId) throws SQLException {
        PreparedStatement ps = con.prepareStatement("select * from monashjss.messages where receiver_id = ?");
        ps.setInt(1, seekerId);
        ResultSet results = ps.executeQuery();

        while (results.next()) {

            int messageId = results.getInt("message_id");
            String sender = results.getString("sender");
            Date date = results.getDate("date");
            String roleName = results.getString("role_name");
            String messageContent = results.getString("message_content");
            Boolean read = results.getBoolean("read");
            int senderId = results.getInt("sender_id");
            int receiverId = results.getInt("receiver_id");

            Message message = new Message(messageId, sender, date, roleName, messageContent, read, senderId, receiverId);
            messageList.add(message);
        }
    }

    public ArrayList<Message> dbGetSeekerMessages(int seekerId) throws SQLException {
        dbLoadSeekerMessages(seekerId);
        return messageList;
    }

    public void dbLoadRecruiterMessages(int recruiterId) throws SQLException {
        String sql = " SELECT `messages`.`message_id`,\n" +
                "            `messages`.`sender`,\n" +
                "            `messages`.`date`,\n" +
                "            `messages`.`role_name`,\n" +
                "            `messages`.`message_content`,\n" +
                "            `messages`.`read`,\n" +
                "            `messages`.`sender_id`,\n" +
                "            `messages`.`receiver_id`\n" +
                "    FROM `monashjss`.`messages`WHERE sender_id = ?";

        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, recruiterId);
        ResultSet results = ps.executeQuery();

        while (results.next()) {
            int messageId = results.getInt("message_id");
            String sender = results.getString("sender");
            Date date = results.getDate("date");
            String roleName = results.getString("role_name");
            String messageContent = results.getString("message_content");
            Boolean read = results.getBoolean("read");
            int senderId = results.getInt("sender_id");
            int receiverId = results.getInt("receiver_id");

            Message message = new Message(messageId, sender, date, roleName, messageContent, read, senderId, receiverId);
            messageList.add(message);
        }
    }

    public ArrayList<Message> dbGetRecruiterMessages(int recruiterId) throws SQLException {
        dbLoadRecruiterMessages(recruiterId);
        return messageList;
    }

    //adds a job seeker appplication to the database
    public void dbAddApp(int jobId, int recruiterId, int seekerId) throws SQLException {

        String insertSql = "INSERT INTO monashjss.job_applications(job_id, recruiter_id, seeker_id,date) values(?,?,?,?)";
        PreparedStatement insertStatement = con.prepareStatement(insertSql);
        Date dateApp = new Date();
        java.sql.Date sqlDate = new java.sql.Date(dateApp.getTime());

        insertStatement.setInt(1, jobId);
        insertStatement.setInt(2, recruiterId);
        insertStatement.setInt(3, seekerId);
        insertStatement.setDate(4, sqlDate);

        insertStatement.executeUpdate();
        insertStatement.close();
        System.out.println("application addedd to db successful");

    }

    //get an arraylist of users that have applied for the specified job
    public ArrayList<User> dbGetJobApplicants(int jobId) throws SQLException {

        ArrayList<User> arrJobApps = new ArrayList<User>();
        String sql = "select * from monashjss.users Right Join monashjss.job_applications on users.id = job_applications.seeker_id WHERE job_id = ?";
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, jobId);
        ResultSet resultSet = ps.executeQuery();

        while (resultSet.next()) {
            int userIdCheck = resultSet.getInt("seeker_id");
            int tempIdCheck = userList.get(0).getId();

            for (int i = 0; i < userList.size(); i++) {
                tempIdCheck = userList.get(i).getId();

                if (userIdCheck == tempIdCheck) {
                    arrJobApps.add(userList.get(i));
                }
            }
        }
        return arrJobApps;
    }

    // login method against the userList that contains usernames + passwords
    public User dbCheckUserLogin(String username, String password) {
        String inputEmail = username.toLowerCase(Locale.ROOT).trim();
        String inputPass = password;
        String checkEmail = "";
        String checkPass = "";
        boolean setIsRecruiter = false;
        int setUserId = 0;

        try {
            for (int i = 0; i < userList.size(); i++) {
                checkEmail = userList.get(i).getEmail().toLowerCase(Locale.ROOT).trim();
                checkPass = userList.get(i).getPassword();

                if ((inputEmail.equals(checkEmail)) && (inputPass.equals(checkPass))) {
                    return userList.get(i);
                }
            }
            System.out.println("No matching user found");
            return null;
        } catch (Exception e) {
            System.out.println("Error, try again");
        }
        return null;
    }

    public int dbAddCategory(String category) throws SQLException {
        int result = -1;

        String insertSql = "INSERT INTO monashjss.job_category(category) values(?)";

        PreparedStatement insertStatement = con.prepareStatement(insertSql);
        insertStatement.setString(1, category);
        result = insertStatement.executeUpdate();
        insertStatement.close();

        System.out.println(result >= 0 ? "Category added to DB" : "Error: category not added to DB");
        return result;
    }

    public void dbAddCategories(ArrayList<String> categories) throws SQLException {
        for (String category : categories)
            dbAddCategory(category);
    }

    public int dbDeleteCategory(int id) throws SQLException {
        int result = -1;

        String sql = "DELETE FROM monashjss.job_category WHERE id = ?";

        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, id);
        result = ps.executeUpdate();
        ps.close();

        System.out.println(result >= 0 ? "Category deleted from DB" : "Error: category not deleted from DB");
        return result;
    }

   /* public void dbDeleteCategories(ArrayList<String> categories) throws SQLException {
        for (String category : categories)
            dbDeleteCategory(category);
    }*/

    public int dbUpdateCategory(int id, String category) throws SQLException {

        int result = -1;
        String sql = "UPDATE monashjss.job_category SET category = ? WHERE id = ? ";
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, category);
        ps.setInt(2, id);
        result = ps.executeUpdate();
        ps.close();

        System.out.println(result > 0 ? "Category updated in DB" : "Error: category not updated in DB");
        return result;
    }

    public void dbUpdateCategories(HashMap<Integer, String> categories) throws SQLException {
        for (Map.Entry<Integer, String> catEntry : categories.entrySet())
            dbUpdateCategory(catEntry.getKey(), catEntry.getValue());
    }

    public void dbLoadCategories() throws SQLException {
        String sql = "SELECT * from monashjss.job_category ORDER BY category ASC";
        Statement selectStatement = con.createStatement();
        ResultSet results = selectStatement.executeQuery(sql);

        while (results.next())
            categories.put(results.getInt("id"), results.getString("category"));
        System.out.println(categories.size() > 0 ? "Categories loaded" : "Error: categories not loaded");
    }

    public void dbLoadTags(boolean seeker, int id) throws SQLException {
        String sql = "SELECT * from monashjss.tags where " + (seeker ? "user_id " : "job_id ") + "= ?";
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet results = ps.executeQuery();

        while (results.next()) {
            TagKey key = new TagKey();
            key.setType(results.getString("type"));
            key.setName(results.getString("name"));
            key.setValue(results.getString("value"));

            Tag tag = new Tag();
            tag.setKey(key);
            tag.setId(results.getInt(seeker ? "user_id" : "job_id"));
            tagsList.add(tag);
        }
    }

    public ArrayList<Job> getRecruiterJobList() throws SQLException {
        return jobsList;
    }
}
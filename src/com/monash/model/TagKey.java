package com.monash.model;

import java.util.Locale;
import java.util.Objects;

/**
 * Tag data class
 * @author   ITO5136-TP6-2021 Team D
 * @version  0.0.1
 */
public class TagKey {
    private String type;
    private String name;
    private String value;

    /**
     * Default constructor
     */
    public TagKey() {
        type = "";
        name = "";
        value = "";
    }

    /**
     * Constructor
     * @param type  Tag type
     * @param name  Tag name
     * @param value Tag value
     */
    public TagKey(String type, String name, String value) {
        this.type = (type == null ? "" : type.toUpperCase(Locale.ROOT));
        this.name = (name == null ? "" : name.toUpperCase(Locale.ROOT));
        this.value = (value == null ? "" : value.toUpperCase(Locale.ROOT));
    }

    /**
     * Get the tag name
     * @return Tag name
     */
    public String getName() {
        return name;
    }

    /**
     * Get the tag type
     * @return Tag type
     */
    public String getType() {
        return type;
    }

    /**
     * Get the tag value
     * @return Tag value
     */
    public String getValue() {
        return value;
    }

    /**
     * Set the tag name
     * @param name Tag name
     */
    public void setName(String name) {
        this.name = name == null ? "" : name.toUpperCase(Locale.ROOT);
    }

    /**
     * Set the tag type
     * @param type Tag type
     */
    public void setType(String type) {
        this.type = type == null ? "" : type.toUpperCase(Locale.ROOT);
    }

    /**
     * Set the tag value
     * @param value Tag value
     */
    public void setValue(String value) {
        this.value = value == null ? "" : value.toUpperCase(Locale.ROOT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TagKey tagData = (TagKey) o;

        return Objects.equals(type, tagData.type) &&
                Objects.equals(name, tagData.name) &&
                Objects.equals(value, tagData.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name, value);
    }

    @Override
    public String toString() {
        return "TagData{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}

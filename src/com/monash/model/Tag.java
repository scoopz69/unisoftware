package com.monash.model;

import java.util.Objects;

/**
 * Tag class
 * @author   ITO5136-TP6-2021 Team D
 * @version  0.0.1
 */
public class Tag {
    private TagKey key;

    private int id;
    private int userId;

    /**
     * Default constructor
     */
    public Tag() {
        id = 0;
        key = new TagKey();
    }

    /**
     * Constructor
     *
     * @param id        Tag ID
     * @param key       Tag key
     */
    public Tag(int id, TagKey key) {
        this();
        this.id = id;
        this.key = key;
        this.userId = 0;
    }

    /**
     * Constructor
     *
     * @param id     Tag ID
     * @param key    Tag key
     * @param userId User ID
     */
    public Tag(int id, TagKey key, int userId) {
        this(id, key);
        this.userId = userId;
    }

    /**
     * Get the user ID
     *
     * @return User ID
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Get the tag data
     * @return Tag key
     */
    public TagKey getKey() {
        return key;
    }

    /**
     * Get the tag ID
     * @return Tag ID
     */
    public int getId() {
        return id;
    }

    /**
     * Set the tag key
     * @param key Tag key
     */
    public void setKey(TagKey key) {
        this.key = key;
    }

    /**
     * Set the tag ID
     * @param id Tag ID
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Set the user ID
     *
     * @param userId User ID
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return id == tag.id && Objects.equals(key, tag.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, key);
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", key=" + key +
                ", userId=" + userId +
                '}';
    }
}

